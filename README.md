# Kotlin State Machines

This is an object-oriented library for the modeling of regular languages. Unlike other popular and faster libraries out there, this one is more comprehensible and intuitive. For instance, you can do this:

```kotlin
val automaton = new DeterministicFiniteAutomaton<Char>()
val newState = State(name = "q0", accepted = false)
automaton.addState(newState)
```

Instead of this crap

```kotlin
val automaton : StringArray = arrayOf(5)
automaton[0] = "q0"
// more array crap ommited for brevity
```

Which is less readable and better at performance, but who gives a f%^&ck about performance these days?

There's also a small visually disgusting application which uses the API to validate user drawn automata

## How to work with these sources

In order to run this project without much pain you will need IntelliJ with the Cucumber for Java plugin which is needed to run the tests. Yes, it has tests.
If you don't have IntelliJ you'll have to download all these dependencies manually:

- cucumber-core-1.2.4
- cucumber-html-0.2.3
- **cucumber-java-1.2.4**
- **cucumber-junit-1.2.4**
- cucumber-jvm-deps-1.0.5
- gherkin-2.12.2
- hamcrest-core-1.3
- **jgraphx-3.4.13**
- junit-4.11

The ones in bold are the real dependencies, all others are sub-dependencies which I'd wish they weren't there bloating the repository's size