package com.jcespinoza.utils

/**
 * Created by jcespinoza on 7/31/2016.
 */
object SetUtils {
    fun <T : Any?> Union(setA : Set<T>, setB : Set<T>) : Set<T>
    {
        val unionSet :MutableSet<T> = mutableSetOf()
        for(element in setA) unionSet.add(element)
        for(element in setB) unionSet.add(element)
        return unionSet
    }

    fun <T : Any?> ToSet(list : List<T>) : Set<T>
    {
        val set : MutableSet<T> = mutableSetOf()
        for(element in list) set.add( element)
        return set
    }

    fun <T : Any?> Union(listOfSets : List<Set<T>>) : Set<T>
    {
        val unionSet : MutableSet<T> = mutableSetOf()
        for(set in listOfSets){
        for(element in set) unionSet.add(element)
    }
        return unionSet
    }

    fun <T : Any?>Intersection( setA : Set<T>, setB : Set<T>) : Set<T>
    {
        val intersectionSet : MutableSet<T> = mutableSetOf()
        val unionSet = Union(setA, setB)
        for(element in unionSet){
            if(setA.contains(element) && setB.contains( element)){
                intersectionSet.add(element)
            }
        }
        return intersectionSet
    }

    fun <T : Any?> Difference(setA : Set<T>, setB : Set<T>) : Set<T>
    {
        val differenceSet : MutableSet<T> = mutableSetOf()
        val unionSet = Union(setA, setB)
        for(element in unionSet){
            if((setA.contains(element) && !setB.contains(element)) || (!setA.contains(element) && setB.contains(element))) {
                differenceSet.add(element)
            }
        }
        return differenceSet
    }
}