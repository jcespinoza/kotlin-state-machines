package com.jcespinoza.io

import com.jcespinoza.gui.model.AutomatonModel

/**
 * Created by jcespinoza on 8/3/2016.
 */
object AutomatonSerializer {
    fun buildString(model : AutomatonModel) : String {
        val builder = StringBuilder()
        builder.append("# meta\n")
                .append("name:")
                .append(model.name + "\n")
                .append("type:")
                .append(model.type + "\n")
                .append("epsilon:")
                .append((if(model.epsilon == null) "" else model.epsilon ) + "\n")
        builder.append("initialState:")
                .append((if (model.initialState == null || model.initialState!!.name!! == null) "" else model.initialState!!.name) + "\n")
                .append("# states\n")
        if(model.stateSet != null && model.stateSet.isNotEmpty()) {
            for (state in model.stateSet) {
                builder.append("name:")
                        .append(state.name + ":")
                        .append("" + (if (state.accepted.equals("true")) "1" else "0") + ":")
                val graphic = model.stateSet.find { p -> p.name == state.name }
                val centerX = "" + (if (graphic != null) graphic.x else "_")
                val centerY = "" + (if (graphic != null) graphic.y else "_")
                builder.append(centerX + "," + centerY + "\n")
            }
        }
        builder.append("# alphabet\n")
        if(model.alphabet != null && model.alphabet.isNotEmpty()) {
            for (sym in model.alphabet) {
                builder.append(sym + "\n")
            }
        }
        builder.append("# final states\n")
        if(model.acceptedStates != null && model.acceptedStates.isNotEmpty()) {
            for (state in model.acceptedStates) {
                builder.append(state.name + "\n")
            }
        }
        builder.append("# transitions\n")
        if(model.transitions != null && model.transitions.isNotEmpty()) {
            for (transition in model.transitions) {
                val symbol = if (transition.symbol.toString().equals(model.epsilon.toString())) model.epsilon.toString() else transition.symbol
                builder.append(symbol + ":")
                        .append(transition.source + ">")
                        .append(transition.target + "\n")
            }
        }
        return builder.toString()
    }
}