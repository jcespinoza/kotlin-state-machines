package com.jcespinoza.io

import com.jcespinoza.gui.model.AutomatonModel
import java.io.*

/**
 * Created by jcespinoza on 9/27/2016.
 */
object AutomatonFileReader {
    fun readFromFile(file : File) : AutomatonModel {
        val reader = BufferedReader(FileReader(file))
        val automatonModel = AutomatonDeserializer.buildFromLines(reader.readLines())
        return automatonModel
    }
}