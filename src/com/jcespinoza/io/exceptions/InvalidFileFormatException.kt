package com.jcespinoza.io.exceptions

/**
 * Created by jcespinoza on 8/2/2016.
 */
class InvalidFileFormatException(message : String = "The given file format is not compatible") : IllegalArgumentException(message)