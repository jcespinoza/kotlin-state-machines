package com.jcespinoza.io

import com.jcespinoza.gui.model.AutomatonModel
import com.mxgraph.model.mxCell
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

/**
 * Created by jcespinoza on 8/2/2016.
 */
object AutomatonFileWriter {
    fun writeToFile(model : AutomatonModel, file : File) : Unit {
        val writer = BufferedWriter(FileWriter(file))
        val content = AutomatonSerializer.buildString(model)
        writer.write(content)
        writer.close()
    }
}