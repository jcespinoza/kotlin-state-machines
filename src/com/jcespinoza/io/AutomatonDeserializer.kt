package com.jcespinoza.io

import com.jcespinoza.gui.model.AutomatonModel
import com.jcespinoza.gui.model.StateModel
import com.jcespinoza.gui.model.TransitionModel
import com.jcespinoza.io.exceptions.InvalidFileFormatException

/**
 * Created by jcespinoza on 8/3/2016.
 */
object AutomatonDeserializer {
    fun buildFromLines(lines : List<String>) : AutomatonModel
    {
        var tail = lines.tail

        if (lines.head != "# meta") {
            throw InvalidFileFormatException()
        }
        //Read meta section
        val namePattern = "name:([A-Za-z0-9_]+)".toRegex()
        val automatonNameMatchResult = namePattern.find(tail.head)
        val automatonName = if (automatonNameMatchResult == null) "" else automatonNameMatchResult.groups[1]!!.value
        tail = tail.tail
        val typePattern = "type:([A-Za-z]+)".toRegex()
        val automatonTypeMatchResult = typePattern.find(tail.head)
        val automatonType = if (automatonTypeMatchResult == null) "" else automatonTypeMatchResult.groups[1]!!.value
        tail = tail.tail
        val epsilonPattern = "epsilon:(.+)".toRegex()
        val epsilonMatchResult = epsilonPattern.find(tail.head)
        val epsilonValue = if(epsilonMatchResult == null) "" else epsilonMatchResult.groups[1]!!.value
        tail = tail.tail
        val initialPattern = "initialState:(.+)".toRegex()
        val initialStateNameMatchResult = initialPattern.find( tail.head)
        val initialStateName = if (initialStateNameMatchResult == null) "" else initialStateNameMatchResult.groups[1]!!.value
        tail = tail.tail
        //Read states section
        if (tail.head != "# states") {
            throw InvalidFileFormatException()
        }
        tail = tail.tail
        val automatonModel = AutomatonModel()
        automatonModel.name = automatonName
        automatonModel.type = automatonType
        automatonModel.epsilon = epsilonValue
        val statePattern = "name:(.+):([01]):(.+),(.+)".toRegex()
        //Read Alphabet section
        while (tail.head != "# alphabet") {
            val statePatterMatchResult = statePattern.find(tail.head)

            val stateName = if(statePatterMatchResult == null ) "" else statePatterMatchResult.groups[1]!!.value
            val acceptedness = if(statePatterMatchResult == null ) "false" else (if (statePatterMatchResult.groups[2]!!.value.equals("1")) "true" else "false")
            val pointX = if(statePatterMatchResult == null ) "" else statePatterMatchResult.groups[3]!!.value
            val pointY = if(statePatterMatchResult == null ) "" else statePatterMatchResult.groups[4]!!.value

            val newStateModel = StateModel(stateName, acceptedness,
                if (pointX != "_") pointX.toDouble() else 150.0,
                if (pointY != "_") pointY.toDouble() else 150.0
            )
            automatonModel.stateSet.add( newStateModel )
            tail = tail.tail
        }
        tail = tail.tail
        //automatonModel.automaton.setStartState(initialStateName)
        //Read final states section
        while (tail.head != "# final states") {
            val sym = tail.head
            automatonModel.alphabet.add(sym)
            tail = tail.tail
        }
        tail = tail.tail
        //Read transitions section
        val acceptedStateNames : MutableList<String> = mutableListOf()
        while (tail.head != "# transitions") {
            acceptedStateNames.add(tail.head)
            tail = tail.tail
        }
        tail = tail.tail
        while (tail.isNotEmpty()) {
            val transitionPattern = "(.+):(.+)>(.+)".toRegex()
            val transitionMatchResult = transitionPattern.find(tail.head)

            val sym = if ( transitionMatchResult == null) "" else transitionMatchResult.groups[1]!!.value
            val sourceName = if ( transitionMatchResult == null) "" else transitionMatchResult.groups[2]!!.value
            val targetName = if ( transitionMatchResult == null) "" else transitionMatchResult.groups[3]!!.value

            val transitionModel = TransitionModel (sourceName, targetName, sym)
            automatonModel.transitions.add( transitionModel )
            tail = tail.tail
        }

        val initialState = automatonModel.stateSet.find { s -> s.name.equals(initialStateName) }
        automatonModel.initialState = initialState
        automatonModel.stateSet.forEach { s ->
            if(s.accepted.equals("true")){
                automatonModel.acceptedStates.add(s)
            }
        }
        return automatonModel
    }

    val <T> List<T>.tail: List<T>
        get() = drop(1)

    val <T> List<T>.head: T
        get() = first()

}