package com.jcespinoza.gui

/**
 * Created by jcespinoza on 7/24/2016.
 */

import com.jcespinoza.logic.AutomatonConstants
import com.mxgraph.model.mxCell
import com.mxgraph.swing.mxGraphComponent
import com.mxgraph.util.mxEvent
import com.mxgraph.util.mxEventObject
import com.mxgraph.util.mxEventSource
import com.mxgraph.util.mxRectangle
import com.mxgraph.view.mxGraph
import javafx.application.Application
import javafx.application.Platform
import javafx.embed.swing.SwingNode
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.FlowPane
import javafx.scene.layout.GridPane
import javafx.stage.FileChooser
import javafx.stage.Stage
import java.io.File
import java.io.IOException

fun main(args: Array<String>) {
    Application.launch(MainApp::class.java)
}

class MainApp : Application() {
    val controller = GraphController()

    // GUI Components
    val testStringField = TextField()
    val evaluateButton = Button("Evaluate")
    val conversionButton = Button("Convert to DFA")
    val minimizationButton = Button("Minimize DFA")
    val loadFileButton = Button("Load Automaton")
    var graph = mxGraph()
    var graphComponent = mxGraphComponent(graph)

    override fun start(stage: Stage) {
        controller.graph = graph

        configureGraph(graph, graphComponent)

        val graphGrid = createMainScreenGrid(stage)

        val sceneRoot = FlowPane(graphGrid)
        stage.scene = Scene(sceneRoot, 900.0, 700.0)
        stage.show()

        initializeAutomaton()
    }

    private fun createMainScreenGrid(stage: Stage): GridPane {
        val connectHandler = mxEventSource.mxIEventListener { sender, event ->
            handleNewTransitionDrawn(event, stage)
        }
        val clickHandler = EventHandler<MouseEvent> {
            handleClickOnGraph(it, stage)
        }
        val evalHandler = EventHandler<MouseEvent> {
            handleEvaluateButtonClick(stage)
        }
        val conversionHandler = EventHandler<MouseEvent>{
            handleConvertButtonClick(stage)
        }
        val minimizationHandler = EventHandler<MouseEvent>{
            handleMinimizationButtonClick(stage)
        }
        val loadFileHandler = EventHandler<MouseEvent> {
            handleLoadfileButtonClick(stage)
        }

        graphComponent.connectionHandler.addListener(mxEvent.CONNECT, connectHandler)
        evaluateButton.onMouseClicked = evalHandler
        conversionButton.onMouseClicked = conversionHandler
        minimizationButton.onMouseClicked = minimizationHandler
        loadFileButton.onMouseClicked = loadFileHandler

        val graphGrid = GridPane()
        val swingNode = SwingNode().apply {
            content = graphComponent
            onMouseClicked = clickHandler
        }

        graphGrid.add(swingNode, 0, 0, 2, 1)
        graphGrid.add(testStringField, 0, 1, 1, 1)
        graphGrid.add(evaluateButton, 1, 1, 1, 1)
        graphGrid.add(conversionButton, 0, 2, 2, 1)
        graphGrid.add(loadFileButton, 0, 3, 2, 1)
        graphGrid.add(minimizationButton, 0, 4, 2, 1)

        graphGrid.onKeyPressed = EventHandler<KeyEvent> {
            if (it?.code == KeyCode.DELETE) {
                controller.handleRemoveSelectedCell()
                graph.model.remove(controller.getSelectedCell())
            } else if (it?.code == KeyCode.I && it?.isControlDown!!) {
                controller.handleSetInitialState()
                graph.refresh()
            }
        }
        return graphGrid
    }

    private fun handleClickOnGraph(it: MouseEvent, stage: Stage) {
        if (it.button != MouseButton.PRIMARY) return
        if (it.clickCount != 2) return

        val cell = graphComponent.getCellAt(it.x.toInt(), it.y.toInt())
        if (cell == null) {
            val stateName = "q" + ++controller.stateCount
//                        val stateName = DialogHelper
//                                .requestStringFromUser("State Name", "Please give a name to the new state", "State Name:")
            val insertedVertex = JGraphHelper.insertVertex(graph, stateName, it.x, it.y)
            val messageContainer = MessageContainer()
            val stateAccepted = controller.handleAddState(insertedVertex as mxCell, messageContainer)

            if (!stateAccepted) {
                graph.model.remove(insertedVertex)
                if (!messageContainer.message.isNullOrBlank()) {
                    Toast.makeText(stage, messageContainer.message, 2500.0, 900.0, 900.0)
                }
            }
        } else {
            val vertex = cell as mxCell
            if (vertex.isVertex) {
                JGraphHelper.toggleAcceptedness(vertex)
                controller.toggleStateAcceptedness(vertex.value as String)
                graph.refresh()
            }
        }
    }

    private fun handleNewTransitionDrawn(event: mxEventObject, stage: Stage) {
        val edge = event.getProperty("cell") as mxCell
        if (edge.isEdge) {
            Platform.runLater({
                assignLabelToEdge(edge)

                val messageContainer = MessageContainer()
                val transitionAccepted = controller.tryToAddTransition(edge, messageContainer)

                if (!transitionAccepted) {
                    graph.model.remove(edge)
                }
                edge.style += "editable=false"

                graph.refresh()
                if (!messageContainer.message.isNullOrBlank()) {
                    Toast.makeText(stage, messageContainer.message, 2500.0, 900.0, 900.0)
                }
            })
        }
    }

    private fun handleEvaluateButtonClick(stage: Stage) {
        val messageContainer = MessageContainer()
        val accepted: Boolean = controller.handleEvaluation(testStringField.text, messageContainer)
        var message = "String is Accepted"
        if (!accepted) {
            message =
                    if (messageContainer.message.isNotEmpty())
                        messageContainer.message
                    else "String is NOT Accepted"
        }
        Toast.makeText(stage, message, 2500.0, 900.0, 900.0)
    }

    private fun handleConvertButtonClick(stage: Stage){
        val messageContainer = MessageContainer()

        val fileChooser = FileChooser()
        fileChooser.title = "Save Converted Automaton"

        fileChooser.initialFileName = "convertedAutomaton01"

        val savedFile = fileChooser.showSaveDialog(stage)
        var message = "Conversion Successful"
        if (savedFile != null) {
            try {
                val converted : Boolean = controller.handleConversion(savedFile, messageContainer)
                if(!converted){
                    message =
                            if (messageContainer.message.isNotEmpty())
                                messageContainer.message
                            else "Unable to convert automaton"
                }else{
                    message = "File saved: " + savedFile.toString()
                }
            }catch(e: IOException) {
                message = "An ERROR occurred while saving the file!"
            }
        }else {
            message = "File save cancelled."
        }

        Toast.makeText(stage, message, 2500.0, 900.0, 900.0)
    }

    private fun handleMinimizationButtonClick(stage: Stage){
        val messageContainer = MessageContainer()

        val fileChooser = FileChooser()
        fileChooser.title = "Save Minimized Automaton"

        fileChooser.initialFileName = "minAutomaton01"

        val savedFile = fileChooser.showSaveDialog(stage)
        var message = "Minimization Successful"
        if (savedFile != null) {
            try {
                val converted : Boolean = controller.handleMinimization(savedFile, messageContainer)
                if(!converted){
                    message =
                            if (messageContainer.message.isNotEmpty())
                                messageContainer.message
                            else "Unable to minimize automaton"
                }else{
                    message = "File saved: " + savedFile.toString()
                }
            }catch(e: IOException) {
                message = "An ERROR occurred while saving the file!"
            }
        }else {
            message = "File save cancelled."
        }

        Toast.makeText(stage, message, 2500.0, 900.0, 900.0)
    }

    private fun handleLoadfileButtonClick(stage: Stage){
        val messageContainer = MessageContainer()

        val fileChooser = FileChooser()
        fileChooser.title = "Open Automaton"

        val openFile = fileChooser.showOpenDialog(stage)
        var message = ""
        if(openFile != null){
            try{
                val loaded : Boolean = controller.loadAutomaton(openFile, messageContainer)
                message = "Load Successful"
                if(!loaded){
                    message =
                            if (messageContainer.message.isNotEmpty())
                                messageContainer.message
                            else "Unable to parse automaton"
                }else{
                    graph.refresh()
                    message = "File Loaded"
                }
            }catch(e: IOException){
                message = "An ERROR occurred while opening the file!"
            }
        }else{
            message = "File open cancelled"
        }

        Toast.makeText(stage, message, 2500.0, 900.0, 900.0)
    }

    private fun assignLabelToEdge(edge: mxCell) {
        val label : String = DialogHelper
                .requestStringFromUser("Transition Symbol", "Please assign a transition symbol", "Symbol:")
        if(!label.isNullOrBlank()){
            edge.value = label
        }
    }

    private fun initializeAutomaton(){
        controller.initializeAutomaton(AutomatonConstants.NFA)
    }

    private fun configureGraph(graph : mxGraph, graphComponent : mxGraphComponent){
        val rect =  mxRectangle(0.0, 0.0, 800.0, 600.0)
        JGraphHelper.configureGraph(graph, graphComponent, rect)
    }

    private fun mxGraph.update(block: () -> Unit) {
        model.beginUpdate()
        try {
            block()
        }
        finally {
            model.endUpdate()
        }
    }

}
