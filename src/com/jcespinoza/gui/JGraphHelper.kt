package com.jcespinoza.gui

import com.mxgraph.model.mxCell
import com.mxgraph.model.mxGeometry
import com.mxgraph.model.mxGraphModel
import com.mxgraph.swing.mxGraphComponent
import com.mxgraph.util.mxConstants
import com.mxgraph.util.mxRectangle
import com.mxgraph.view.mxGraph
import java.awt.Color

/**
 * Created by jcespinoza on 8/4/2016.
 */

object JGraphHelper {

    fun getAllCells(graph : mxGraph?): List<mxCell> {
        val allChildren = mxGraphModel.getChildCells(graph!!.model, graph.defaultParent, true, true).toList()
        return allChildren as List<mxCell>
    }

    fun getAllVertices(graph : mxGraph?): List<mxCell> {
        val allCells = getAllCells(graph)
        if (allCells.isEmpty()) return listOf()
        return allCells.filter { c -> c.isVertex }
    }

    fun getAllEdges(graph : mxGraph?): List<mxCell> {
        val allCells = getAllCells(graph)
        if (allCells.isEmpty()) return listOf()
        return allCells.filter { c -> c.isEdge }
    }

    fun getSelectedCell(graph : mxGraph?) : mxCell?{
        return graph!!.selectionModel.cells.find {c -> graph.selectionModel.isSelected(c) } as mxCell?
    }

    fun setInitialStateStyle(graph: mxGraph?, targetVertex: mxCell) {
        for(vertex in getAllVertices(graph)) {
            val newStyle = vertex.style.replace("fillColor=gray", "fillColor=white")
            vertex.style = newStyle
        }
        targetVertex.style = targetVertex.style.replace("fillColor=white", "fillColor=gray")
    }

    fun clearGraph(graph: mxGraph?){
        graph!!.removeCells()
    }

    fun toggleAcceptedness(vertex: mxCell) {
        val oldStyle = vertex.style
        var newStyle = oldStyle.replace("shape=ellipse", "shape=doubleEllipse")
        if(oldStyle.contains("=doubleEllipse")){
            newStyle = oldStyle.replace("shape=doubleEllipse", "shape=ellipse")
        }
        vertex.style = newStyle
    }

    fun configureGraph(graph : mxGraph, graphComponent : mxGraphComponent, rect : mxRectangle) {
        graph.isAllowLoops = true
        graph.isAllowDanglingEdges = false
        graph.isEdgeLabelsMovable = false
        graph.minimumGraphSize = rect
        graph.isCollapseToPreferredSize = false
        mxConstants.DEFAULT_HOTSPOT = 1.0
        graph.isCellsResizable = false
        graphComponent.isEnterStopsCellEditing = true
        graphComponent.isFocusable = true
        graphComponent.background = Color.WHITE
        graph.selectionModel.isSingleSelection = true
        graph.isCellsCloneable = false
        graph.model.setGeometry(graph.defaultParent, mxGeometry(rect.x, rect.y, rect.width, rect.height))
    }

    fun insertVertex(graph: mxGraph, stateName: String, x: Double, y: Double): Any? {
        return graph.insertVertex(graph.defaultParent, null, stateName, x - 25.0, y - 25.0, 50.0, 50.0, "shape=ellipse;fillColor=white;editable=false")
    }
}