package com.jcespinoza.gui

import javafx.scene.control.TextInputDialog

/**
 * Created by jcespinoza on 8/7/2016.
 */
object DialogHelper{
    fun requestStringFromUser(title: String, header : String, caption: String): String {
        val dialog = TextInputDialog()
        dialog.title = title
        dialog.headerText = header
        dialog.contentText = caption

        val result = dialog.showAndWait()
        if (result.isPresent) {
            return result.get()
        }
        return ""
    }
}