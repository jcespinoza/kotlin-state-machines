package com.jcespinoza.gui.model

/**
 * Created by jcespinoza on 8/2/2016.
 */
class StateModel(){
    var name: String = ""
    var accepted: String = "false"
    var x: Double = 0.0
    var y: Double = 0.0
    constructor(name: String, accepted : String, x : Double, y : Double) : this(){
        this.name = name
        this.accepted = accepted
        this.x = x
        this.y = y
    }

}