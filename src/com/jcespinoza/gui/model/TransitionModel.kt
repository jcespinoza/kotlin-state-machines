package com.jcespinoza.gui.model

/**
 * Created by jcespinoza on 8/2/2016.
 */
class TransitionModel(){
    var source: String = ""
    var target: String = ""
    var symbol: String = ""

    constructor(source: String, target: String, symbol: String) : this(){
        this.source = source
        this.target = target
        this.symbol = symbol
    }
}