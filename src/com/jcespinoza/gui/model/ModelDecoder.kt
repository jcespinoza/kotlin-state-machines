package com.jcespinoza.gui.model

import com.jcespinoza.logic.*
import com.jcespinoza.logic.exceptions.InvalidAutomatonException
import com.mxgraph.model.mxCell
import com.mxgraph.view.mxGraph

/**
 * Created by jcespinoza on 8/5/2016.
 */
object ModelDecoder{
    fun decodeModel(model: AutomatonModel, graph : mxGraph) : IAutomaton<Char>?{
        val automaton = AutomatonInstanceProvider.createInstance(model.type, Symbol('ε'))

        if(automaton == null) InvalidAutomatonException()

        for (sym in model.alphabet){
            automaton!!.addSymbolToAlphabet(Symbol(sym[0]))
        }

        val vertices : MutableList<mxCell> = mutableListOf()

        for(st in model.stateSet){
            val newState = State(st.name, st.accepted.equals("true"))
            val newVertex = graph.insertVertex(graph.defaultParent,
                    st.name,
                    st.name,
                    st.x,
                    st.y,
                    50.0,
                    50.0,
                    (if (newState.isAccepted) "shape=doubleEllipse;" else "shape=ellipse;")
                            + ( if(model.initialState!!.name == st.name) "fillColor=gray;" else "fillColor=white;" )+

                            "editable=false")
            vertices.add(newVertex as mxCell)
            automaton!!.addState(newState)
        }

        if(model.initialState != null) {
            automaton!!.setStartState(model.initialState!!.name)
        }

        for(st in model.acceptedStates){
            automaton!!.addAcceptedState(st.name)
        }

        for(tran in model.transitions){
            val originState = automaton!!.getStateByName(tran.source)
            val originVertex = vertices.find { v -> v.value.equals(tran.source) }
            val targetState = automaton.getStateByName(tran.target)
            val targetVertex = vertices.find { v -> v.value.equals(tran.target) }
            val newTransition = StateTransition(originState, targetState, Symbol(tran.symbol[0]))

            automaton.addStateTransition(newTransition)
            graph.insertEdge(graph.defaultParent,
                    null,
                    tran.symbol,
                    originVertex,
                    targetVertex,
                    "editable=false")
        }

        return automaton
    }
}