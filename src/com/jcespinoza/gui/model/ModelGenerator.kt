package com.jcespinoza.gui.model

import com.jcespinoza.gui.JGraphHelper
import com.jcespinoza.logic.*
import com.jcespinoza.logic.exceptions.InvalidAutomatonException
import com.mxgraph.view.mxGraph
import com.sun.javaws.exceptions.InvalidArgumentException
import org.jetbrains.annotations.Mutable

/**
 * Created by jcespinoza on 8/4/2016.
 */
object ModelGenerator{
    fun GenerateModel(automaton : IAutomaton<Char>, graph : mxGraph, name: String) : AutomatonModel {
        val automatonModel = AutomatonModel()
        automatonModel.name = name
        automatonModel.type = getAutomatonType(automaton)
        automatonModel.epsilon = "ε"
        automatonModel.alphabet = getAutomatonAlphabet(automaton)
        automatonModel.stateSet = getAutomatonStates(automaton, graph)
        automatonModel.initialState = getInitialStateModel(automaton, automatonModel.stateSet)
        automatonModel.acceptedStates = getAcceptedStateModels(automaton, automatonModel.stateSet)
        automatonModel.transitions = getAutomatonTransitions(automaton)
        return automatonModel
    }

    private fun getAcceptedStateModels(automaton: IAutomaton<Char>, stateSet: MutableList<StateModel>): MutableList<StateModel> {
        val acceptedStates = automaton.getAcceptedStates()
        val listOfModels : MutableList<StateModel> = mutableListOf()
        for (st in acceptedStates){
            val foundModel = stateSet.find { s -> s.name.equals(st.getStateName()) }
                    ?: throw InvalidAutomatonException("Could not find state")

            listOfModels.add(foundModel)
        }
        return listOfModels
    }

    private fun getAutomatonTransitions(automaton: IAutomaton<Char>): MutableList<TransitionModel> {
        val listOfTransitions : MutableList<TransitionModel> = mutableListOf()
        automaton.getTransitions().forEach { t ->
            val newTransition = TransitionModel(t.originState!!.getStateName(), t.targetState!!.getStateName(), t.triggerSymbol.toString())
            listOfTransitions.add(newTransition)
        }
        return listOfTransitions
    }

    private fun getAutomatonAlphabet(automaton: IAutomaton<Char>): MutableList<String> {
        val listOfSymbols : MutableList<String> = mutableListOf()
        automaton.getAlphabet().forEach { s -> listOfSymbols.add(s.innerSymbol.toString()) }
        return listOfSymbols
    }

    private fun getInitialStateModel(automaton: IAutomaton<Char>, stateSet: MutableList<StateModel>): StateModel? {
        val initialState = automaton.getStartState() ?: return null

        val foundModel = stateSet.find { s -> s.name.equals(initialState.getStateName()) }

        return foundModel
    }

    private fun getAutomatonStates(automaton: IAutomaton<Char>, graph: mxGraph): MutableList<StateModel> {
        val vertices = JGraphHelper.getAllVertices(graph)
        val states = automaton.getStates()

        val listOfStates : MutableList<StateModel> = mutableListOf()
        for(st in states){
            val graphicVertex = vertices.find { c -> c.value.equals(st.getStateName()) }
            val newModel = StateModel()
            newModel.name = st.getStateName()
            newModel.accepted = if (st.isAccepted) "true" else "false"
            if(graphicVertex != null) {
                newModel.x = graphicVertex!!.geometry.x
                newModel.y = graphicVertex.geometry.y
            }else{
                newModel.x = (5 + (Math.random() * ((400 - 5) + 1)) )
                newModel.y = (5 + (Math.random() * ((400 - 5) + 1)) )
            }
            listOfStates.add(newModel)
        }
        return listOfStates
    }

    private fun getAutomatonType(automaton: IAutomaton<Char>): String {
        return when (automaton){
            is DeterministicFiniteAutomaton<Char> -> AutomatonConstants.DFA
            is NonDeterministicFiniteAutomaton<Char> -> AutomatonConstants.NFA
            is EpsilonNonDeterministicFiniteAutomaton<Char> -> AutomatonConstants.eNFA
            else -> throw InvalidAutomatonException("Unable to determine automaton type")
        }
    }
}