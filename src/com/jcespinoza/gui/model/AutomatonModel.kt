package com.jcespinoza.gui.model

import com.jcespinoza.logic.IAutomaton

/**
 * Created by jcespinoza on 8/2/2016.
 */
class AutomatonModel{
    var automaton : IAutomaton<Char>? = null
    var name: String = ""
    var type: String = "DFA"
    var epsilon: String = "ε"
    var stateSet: MutableList<StateModel> = mutableListOf()
    var alphabet : MutableList<String> = mutableListOf()
    var initialState : StateModel? = null
    var acceptedStates : MutableList<StateModel> = mutableListOf()
    var transitions : MutableList<TransitionModel> = mutableListOf()
}