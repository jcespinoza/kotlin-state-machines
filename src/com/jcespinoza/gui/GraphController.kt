package com.jcespinoza.gui

import com.jcespinoza.gui.model.AutomatonModel
import com.jcespinoza.gui.model.ModelDecoder
import com.jcespinoza.gui.model.ModelGenerator
import com.jcespinoza.io.AutomatonFileReader
import com.jcespinoza.io.AutomatonFileWriter
import com.jcespinoza.logic.*
import com.jcespinoza.logic.exceptions.DuplicateStateException
import com.jcespinoza.logic.exceptions.DuplicateTransitionForStateAndSymbolException
import com.mxgraph.model.mxCell
import com.mxgraph.model.mxGraphModel
import com.mxgraph.view.mxGraph
import java.io.File

/**
 * Created by jcespinoza on 7/25/2016.
 */
class GraphController () {
    var graph : mxGraph? = null
    var stateCount: Int = 0
    private var automaton : IAutomaton<Char>? = null

    fun setAutomatonObject(automaton : IAutomaton<Char>) : Unit {
        this.automaton = automaton
    }

    fun initializeAutomaton(automatonType : String) : Unit {
        stateCount = 0
        automaton = AutomatonInstanceProvider.createInstance(automatonType, Symbol('ε'))
    }

    fun getSelectedCell(): mxCell? {
        return JGraphHelper.getSelectedCell(graph)
    }

    fun tryToAddTransition(edge: mxCell, messageContainer: MessageContainer) : Boolean {
        try {
            val sourceState = automaton!!.getStateByName(edge.source.value as String)
            val targetState = automaton!!.getStateByName(edge.target.value as String)
            val symbol = Symbol((edge.value as String)[0])
            val transition = StateTransition(sourceState, targetState, symbol)
            automaton!!.addSymbolToAlphabet(symbol)
            automaton!!.addStateTransition(transition)
            return true
        }catch (ex: DuplicateTransitionForStateAndSymbolException){
            messageContainer.message = ex.message!!
        }catch(ex:  StringIndexOutOfBoundsException){
            messageContainer.message = "The transition symbol is required"
        }
        return false
    }

    fun handleRemoveSelectedCell() {
        val selectedCell = getSelectedCell()
        if(selectedCell?.isEdge ?:false ){
            handleRemoveTransition(selectedCell)
        }
        if(selectedCell?.isVertex ?: false){
            handleRemoveState(selectedCell)
        }
    }

    private fun handleRemoveState(selectedCell: mxCell?){
        val stateFound = automaton!!.getStateByName(selectedCell!!.value as String)
        automaton!!.removeState(stateFound)
    }

    private fun handleRemoveTransition(selectedCell: mxCell?) {
        val sourceState = automaton!!.getStateByName(selectedCell!!.source.value as String)
        val targetState = automaton!!.getStateByName(selectedCell.target.value as String)
        val symbol = Symbol((selectedCell.value as String)[0])

        val transitionFound = automaton!!.getTransitions().firstOrNull {t -> t.originState!!.equals(sourceState)
            && t.triggerSymbol!!.equals(symbol) && t.targetState!!.equals(targetState)}
        automaton!!.removeStateTransition(transitionFound)
    }

    fun handleAddState(vertex: mxCell?, messageContainer: MessageContainer) : Boolean {
        val state = State(vertex!!.value as String, false)
        try{
            automaton!!.addState(state)
            return true
        }catch (ex : DuplicateStateException){
            messageContainer.message = ex.message!!
            return false
        }
    }

    fun getAllCells() : List<mxCell>{
        return JGraphHelper.getAllCells(graph)
    }

    fun getAllVertices() : List<mxCell>{
        return JGraphHelper.getAllVertices(graph)
    }

    fun getAllEdges() : List<mxCell>{
        return JGraphHelper.getAllEdges(graph)
    }

    fun toggleStateAcceptedness(stateName: String) {
        val stateFound : State? = automaton!!.getStateByName(stateName)
        if(stateFound != null){
            stateFound.isAccepted = !stateFound.isAccepted
            if(stateFound.isAccepted){
                (automaton as AutomatonBase).addAcceptedState(stateFound)
            }else{
                (automaton as AutomatonBase).removeAcceptedState(stateFound)
            }
        }
    }

    fun handleEvaluation(text: String?, messageContainer: MessageContainer): Boolean {
        if(!automaton!!.isValid()){
            messageContainer.message = "Automaton is not complete"
            return false
        }
        val symbolList : MutableList<Symbol<Char>> = mutableListOf()
        text!!.forEach { s ->
            val symbol = Symbol(s, false)
            symbol.isEpsilon = s == AutomatonConstants.epsilon
            symbolList.add(symbol)
        }

        return automaton!!.acceptsInput(symbolList)
    }

    fun handleSetInitialState() {
        val selectedVertex = getSelectedCell()
        if(selectedVertex != null && selectedVertex.isVertex){
            JGraphHelper.setInitialStateStyle(graph, selectedVertex)
            automaton!!.setStartState(selectedVertex.value as String)
        }
    }

    fun handleConversion(file: File, messageContainer: MessageContainer): Boolean {
        if(automaton == null){
            messageContainer.message = "No automaton to convert"
            return false
        }
        val resultingAutomaton = AutomataConverter.ConvertToDFA(automaton!!)
        val targetModel = ModelGenerator.GenerateModel(resultingAutomaton, mxGraph(), file.name)

        AutomatonFileWriter.writeToFile(targetModel, file)

        return true
    }

    fun handleMinimization(file : File, messageContainer: MessageContainer): Boolean {
        if(automaton == null){
            messageContainer.message = "No automaton to minimize"
            return false
        }
        val resultingAutomaton = DFAMinimizer.Minimize(automaton!!)
        val targetModel = ModelGenerator.GenerateModel(resultingAutomaton, mxGraph(), file.name)

        AutomatonFileWriter.writeToFile(targetModel, file)

        return true
    }

    fun loadAutomaton(file: File, messageContainer: MessageContainer): Boolean {
        val model = AutomatonFileReader.readFromFile(file)
        val allCells = getAllCells().forEach {
            graph!!.model.remove(it)
        }
        val decodedAutomaton = ModelDecoder.decodeModel(model, graph!!)

        automaton = decodedAutomaton
        return true
    }
}