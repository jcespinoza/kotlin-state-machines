package com.jcespinoza.logic

import com.jcespinoza.logic.exceptions.DuplicateTransitionForStateAndSymbolException

/**
 * Created by jcespinoza on 7/24/2016.
 */
class DeterministicFiniteAutomaton<T : Any?>() : AutomatonBase<T>(){

    fun getTransitionForState(state: State, symbol: Symbol<T>) : StateTransition<T>? {
        val transitions = getTransitionsForStateAndSymbol(state, symbol)
        if( transitions.isEmpty()) return null
        return transitions.first()
    }

    override fun addStateTransition(tran: StateTransition<T>): Unit
    {
        val existingTransition = getTransitionForState(tran.originState!!, tran.triggerSymbol!!)
        if(existingTransition != null) throw DuplicateTransitionForStateAndSymbolException()
        transitions .add(tran)
    }

    override fun acceptsInput(symbolString: List<Symbol<T>>): Boolean
    {
        var currentState = _startState
        for(symbol in symbolString ){
            if(!acceptsSymbol(symbol)) return false

            val transition = getTransitionForState(currentState!!, symbol)
            if(transition == null) return false

            currentState = transition.targetState
        }
        return currentState!!.isAccepted || acceptedStates.contains(currentState)
    }

    override fun isValid(): Boolean
    {
        return super.isValid() &&
                !alphabet.any {s -> s.isEpsilon}
    }

    override fun addSymbolToAlphabet(sym: Symbol<T>): Unit {
        alphabet.add(sym)
    }
}