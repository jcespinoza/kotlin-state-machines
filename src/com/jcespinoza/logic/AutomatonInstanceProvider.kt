package com.jcespinoza.logic

/**
 * Created by jcespinoza on 8/5/2016.
 */
object AutomatonInstanceProvider{
    fun <T: Any?> createInstance(automatonType: String, epsilon: Symbol<T>): IAutomaton<T>? {
        when (automatonType) {
            AutomatonConstants.DFA -> return DeterministicFiniteAutomaton<T>()
            AutomatonConstants.NFA -> return NonDeterministicFiniteAutomaton<T>()
            AutomatonConstants.eNFA -> return EpsilonNonDeterministicFiniteAutomaton(epsilon)
            else -> return null
        }
    }
}