package com.jcespinoza.logic

/**
 * Created by jcespinoza on 7/24/2016.
 */
class Symbol<T : Any?> (sym  : T, isEps : Boolean = false){
    companion object {
        inline operator fun <reified T : Any>invoke() = Symbol(T::class.java)
    }

    constructor(sym : T) : this(sym, false){}

    var innerSymbol: T = sym
    var isEpsilon: Boolean = isEps

    override fun equals(other: Any?): Boolean {
        when (other) {
            is Symbol<*> -> {
                (other.canEqual(this)) &&
                        return (if (innerSymbol != null) innerSymbol?.equals(other.innerSymbol) ?: false else false)

            }
        }
        return false
    }

    private fun canEqual(other: Any?): Boolean {
        return other is Symbol<*>
    }

    override fun hashCode(): Int
    {
        return 7 * (7 + (innerSymbol?.hashCode() ?: 0) )
    }
    override fun toString() : String{
        return if (innerSymbol != null) innerSymbol.toString() else ""
    }
}