package com.jcespinoza.logic

import java.util.*

/**
 * Created by jcespinoza on 8/2/2016.
 */
class StateGroup() : State(){
    var innerStates : SortedSet<State> = sortedSetOf()
    override fun getStateName() : String {
        val strBuilder = StringBuilder()
        strBuilder.append( "{" )
        for(state in innerStates){
            if(state != innerStates.first()) strBuilder.append( ",")
            strBuilder.append( state.getStateName() )
        }
        strBuilder.append(  "}" )
        return strBuilder.toString()
    }

    fun hasState(state : State) : Boolean {
        if(state.javaClass.equals(StateGroup)) return false
        innerStates.forEach { i -> if(i.equals(state)) return true }
        return false
    }

    companion object Group{
        fun CreateGroup(states : List<State>) : StateGroup {
            val stateGroup = StateGroup()

            var foundAccepting = false

            for(state in states){
                // I KNOW WHAT YOU'RE THINKING
                // IT'S BAD PRACTICE TO CHECK FOR SUBTYPES
                // BUT GUESS WHO DOESN'T GIVE A FUCK?
                if(state is StateGroup){
                    for(st in state.innerStates){
                        if(st.isAccepted){ foundAccepting = true }
                        stateGroup.innerStates.add( st )
                    }
                }else if(state is State){
                    if(state.isAccepted){ foundAccepting = true }
                    stateGroup.innerStates.add( state )
                }
            }
            stateGroup.isAccepted = foundAccepting
            return stateGroup
        }

        fun CreateGroupFromGroups(stateGroups : List<StateGroup>) : StateGroup {
            val stateGroup = StateGroup()
            var foundAccepting = false

            for(group in stateGroups){
                for(state in group.innerStates){
                    if(state.isAccepted){ foundAccepting = true }
                    stateGroup.innerStates.add( state )
                }
            }
            stateGroup.isAccepted = foundAccepting
            return stateGroup
        }

        fun CreateGroup(state : State) : StateGroup? {
            when (state) {
                is StateGroup -> return state
                is State -> {
                    val stateGroup = StateGroup()
                    stateGroup.isAccepted = state.isAccepted
                    stateGroup.innerStates.add(state)
                    return stateGroup
                }
                else -> return null
            }
        }
    }
}

