package com.jcespinoza.logic

/**
 * Created by jcespinoza on 7/24/2016.
 */
class StateTransition <T : Any?>constructor(){
    var originState : State? = null
    var targetState: State? = null
    var triggerSymbol : Symbol<T>? = null

    constructor(origin : State?, destination : State?, trigger : Symbol<T>?) : this(){
        this.originState = origin
        this.targetState = destination
        this.triggerSymbol = trigger
    }

    override fun hashCode(): Int
    {
        val symHash = if (triggerSymbol == null)  0 else triggerSymbol?.hashCode()
        val oriHash = if (originState == null ) 0 else originState?.hashCode()
        val destHash = if (targetState == null) 0 else targetState?.hashCode()

        return 7 * (7 * (7 + (destHash ?: 0)) + (oriHash ?: 0) ) + (symHash ?: 0)
    }

    private fun canEqual(other  : Any) : Boolean {
        return other is StateTransition<*>
    }

    override fun equals(other : Any?) : Boolean
    {
        when (other) {
            is StateTransition<*> -> {
                val that = other
                val canEqual = (that.canEqual(this) )
                val symbolsAreEqual = (if (triggerSymbol != null) triggerSymbol?.equals(that.triggerSymbol) ?: false else false)
                val targetsAreEqual = (if (targetState != null) targetState?.equals(that.targetState) ?: false else false)
                val sourcesAreEqual = (if (originState != null) originState?.equals(that.originState) ?: false else false)
                 return canEqual && symbolsAreEqual &&
                        targetsAreEqual && sourcesAreEqual
            }
        }
        return false
    }

    override fun toString() : String
    {
        return "{originState: " + originState +
                ", destination: " + targetState +
                ", triggerSymbol: " + triggerSymbol + "}"
    }
}