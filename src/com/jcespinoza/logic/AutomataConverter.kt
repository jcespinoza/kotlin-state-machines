package com.jcespinoza.logic

import com.jcespinoza.logic.common.AutomatonValidator
import com.jcespinoza.logic.exceptions.UnsupportedConversionException
import com.jcespinoza.utils.SetUtils
import java.util.*

/**
 * Created by jcespinoza on 8/2/2016.
 */
object AutomataConverter {

    fun <T : Any?> ConvertToDFA(original : IAutomaton<T>) : DeterministicFiniteAutomaton<T>
    {
        AutomatonValidator.ThrowIfAutomatonIsInvalid(original)
        when (original) {
            is DeterministicFiniteAutomaton <T> -> return original
            is NonDeterministicFiniteAutomaton <T> -> return ConvertNFAtoDFA(original)
            is EpsilonNonDeterministicFiniteAutomaton <T> -> return ConvertENFAtoDFA(original)
            else -> throw UnsupportedConversionException()
        }
    }

    private fun <T> ConvertENFAtoDFA(original: EpsilonNonDeterministicFiniteAutomaton<T>): DeterministicFiniteAutomaton<T> {
        val klosedInitialStates = original.evaluateKleeneClosure(original.getStartState()).toList() as List<State>
        val initialState = StateGroup.CreateGroup(klosedInitialStates)
        return createDeterministicAutomaton(initialState, original,
                { automaton, state, symbol -> getKlosedDestinationsWithSymbol(automaton, state, symbol) })
    }

    private fun <T : Any?> ConvertNFAtoDFA(original: NonDeterministicFiniteAutomaton<T>): DeterministicFiniteAutomaton<T> {
        val initialState = StateGroup.CreateGroup(listOf(original.getStartState()))
        return createDeterministicAutomaton(initialState, original,
                                        {automaton, state,symbol -> getDestinationWithSymbol(automaton, state, symbol)})
    }

    private fun <T> createDeterministicAutomaton(initialState: StateGroup,
                                                 original: IAutomaton<T>,
                                                 deltaFunction: (IAutomaton<T>, StateGroup, Symbol<T>)
                                                    -> StateGroup?): DeterministicFiniteAutomaton<T>
    {
        var currentStates: MutableSet<StateGroup> = mutableSetOf(initialState)
        var notProcessedStates: MutableSet<StateGroup> = mutableSetOf()
        val processedStates: MutableSet<StateGroup> = mutableSetOf()

        val resultingDFA = createResultingDFA(initialState, original)

        do {
            for (group in currentStates) {
                processedStates.add(group)
                for (sym in original.getAlphabet()) {
                    val newStateGroup = deltaFunction(original, group, sym)
                    addStateAndTransitionIfValid(group, newStateGroup, notProcessedStates, processedStates, resultingDFA, sym)
                }
            }
            currentStates = SetUtils.Union(listOf(notProcessedStates)) as MutableSet<StateGroup>
            notProcessedStates = mutableSetOf()
        } while (currentStates.isNotEmpty())

        return resultingDFA
    }

    private fun <T> createResultingDFA(initialState: StateGroup, original: IAutomaton<T>): DeterministicFiniteAutomaton<T> {
        val resultingDFA = DeterministicFiniteAutomaton<T>()
        resultingDFA.setAlphabet(original.getAlphabet())
        resultingDFA.addState(initialState)
        resultingDFA.setStartState(initialState)
        return resultingDFA
    }

    private fun <T> addStateAndTransitionIfValid(group: StateGroup, newStateGroup: StateGroup?,
                                                 notProcessedStates: MutableSet<StateGroup>, processedStates: MutableSet<StateGroup>,
                                                 resultingDFA: DeterministicFiniteAutomaton<T>, sym: Symbol<T>) {
        if (newStateGroup != null) {

            val newOrExistingState = getSimilarStateGroup(newStateGroup, resultingDFA)
            val transition = StateTransition(group, newOrExistingState, sym)
            if(resultingDFA.getTransitionsForStateAndSymbol(group, sym).isEmpty()) {
                resultingDFA.addStateTransition(transition)
            }
            if (resultingDFA.getStateByName(newOrExistingState.getStateName()) == null) {
                resultingDFA.addState(newOrExistingState)
            }
            if (newOrExistingState.isAccepted){
                resultingDFA.addAcceptedState(newOrExistingState)
            }
            if (!processedStates.contains(newStateGroup)) {
                notProcessedStates.add(newOrExistingState)
            }
        }
    }

    private fun <T: Any?> getDestinationWithSymbol(automaton: IAutomaton<T>, stateGroup: StateGroup, symbol: Symbol<T>) : StateGroup?
    {
        val destinationStates : SortedSet<State> = sortedSetOf()

        for(state in stateGroup.innerStates){
            for(s in automaton.getTransitionsForStateAndSymbol(state, symbol)) {
                destinationStates.add( s!!.targetState )
            }
        }
        if(destinationStates.isEmpty()) return null
        return StateGroup.CreateGroup(destinationStates.toList())
    }

    private fun <T> getKlosedDestinationsWithSymbol(automaton: IAutomaton<T>, stateGroup: StateGroup, symbol: Symbol<T>): StateGroup? {
        val destinationStates : SortedSet<State> = sortedSetOf()

        for(state in stateGroup.innerStates){
            for(s in automaton.getTransitionsForStateAndSymbol(state, symbol)) {
                val klosedStates = (automaton as EpsilonNonDeterministicFiniteAutomaton).evaluateKleeneClosure(s!!.targetState)
                for(k in klosedStates){
                    destinationStates.add( k )
                }
            }
        }
        if(destinationStates.isEmpty()) return null
        return StateGroup.CreateGroup(destinationStates.toList())
    }


    private fun <T: Any?> getSimilarStateGroup(stateGroup: StateGroup, dfa: DeterministicFiniteAutomaton<T>) : StateGroup {
        val existingStateGroup = dfa.getStateByName(stateGroup.getStateName())
        if(existingStateGroup != null) return existingStateGroup as StateGroup
        return stateGroup
    }
}