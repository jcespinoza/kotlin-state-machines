package com.jcespinoza.logic

/**
 * Created by jcespinoza on 7/24/2016.
 */

interface IAutomaton<T : Any?> {
    fun addSymbolToAlphabet(sym: Symbol<T>) : Unit
    fun getAlphabet(): List<Symbol<T>>
    fun setAlphabet(alph: List<Symbol<T>>)
    fun getStates(): List<State>
    fun getStateByName(name : String) : State?
    fun addState(st : State?) : Unit
    fun removeState(st : State?) : Unit
    fun removeState(name : String) : Unit
    fun getStartState(): State?
    fun setStartState(st : State?)
    fun setStartState(name : String)
    fun addAcceptedState(st: String) : Unit
    fun addAcceptedState(st : State) : Unit
    fun removeAcceptedState(st : String) : Unit
    fun removeAcceptedState(st : State?) : Unit
    fun getAcceptedStates(): List<State>
    fun getTransitions(): List<StateTransition<T>>
    fun addStateTransition(tran : StateTransition<T>) : Unit
    fun removeStateTransition(tran : StateTransition<T>?) : Unit
    fun getTransitionsForState(st : State?) : List<StateTransition<T>>
    fun getTransitionsForStateAndSymbol(st : State?, sym : Symbol<T>) : List<StateTransition<T>?>
    fun acceptsInput(symbolString : List<Symbol<T>>) : Boolean
    fun acceptsSymbol(sym : Symbol<T>) : Boolean
    fun isValid(): Boolean
}