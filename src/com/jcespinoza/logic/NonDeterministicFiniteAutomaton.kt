package com.jcespinoza.logic

import com.jcespinoza.utils.SetUtils

/**
 * Created by jcespinoza on 7/31/2016.
 */
class NonDeterministicFiniteAutomaton<T : Any?> : AutomatonBase<T>() {

    fun evaluateDestinationsForSymbol(stateList: Set<State?>, symbol: Symbol<T>) : List<Set<State?>> {
        var listOfSets : MutableList<MutableSet<State?>> = mutableListOf()
        for(state in stateList){
            var destinationStates : MutableSet<State?> = mutableSetOf()
            val transitions = getTransitionsForStateAndSymbol(state!!, symbol)

            for(transition in transitions){
                destinationStates.add( transition.targetState)
            }

            listOfSets.add( destinationStates )
        }
        return listOfSets
    }

    override fun acceptsInput(symbolString: List<Symbol<T>>): Boolean
    {
        var currentStates : Set<State?> = mutableSetOf(_startState)
        for(symbol in symbolString){
        if(!acceptsSymbol(symbol)) return false

        val nextStates = evaluateDestinationsForSymbol(currentStates, symbol)
        currentStates = SetUtils.Union(nextStates)
    }

        return currentStates.intersect(acceptedStates).any()
    }

    override fun isValid(): Boolean
    {
        return super.isValid() &&
                !alphabet.any {s -> s.isEpsilon}
    }

    override fun addSymbolToAlphabet(sym: Symbol<T>): Unit {
        alphabet .add( sym )
    }
}