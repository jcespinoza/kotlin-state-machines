package com.jcespinoza.logic.common

import com.jcespinoza.logic.IAutomaton
import com.jcespinoza.logic.exceptions.IncompatibleAutomataException
import com.jcespinoza.logic.exceptions.InvalidAutomatonException

/**
 * Created by jcespinoza on 8/2/2016.
 */
object AutomatonValidator {
    fun <T : Any?> ThrowIfNotOfTheSameType(automatonA: IAutomaton<T>?, automatonB: IAutomaton<T>?) : Unit {
        if(automatonA == null || automatonB == null) return
        if(automatonA.javaClass != automatonB.javaClass){
            throw IncompatibleAutomataException()
        }
    }

    fun <T : Any?> ThrowIfAutomatonIsInvalid(automaton: IAutomaton<T>?) : Unit {
        if (automaton == null) throw IllegalArgumentException("The original automaton can not be null")
        val valid = automaton.isValid()
        if (!valid) throw InvalidAutomatonException()
    }
}