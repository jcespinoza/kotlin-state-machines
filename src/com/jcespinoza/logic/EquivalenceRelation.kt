package com.jcespinoza.logic

/**
 * Created by jcespinoza on 8/24/2016.
 */
class EquivalenceRelation(var state1: State?, var state2 : State?, var areEquivalent : Boolean){
    override fun equals(other: Any?) : Boolean {
        return when (other) {
            is EquivalenceRelation ->
            (other.canEqual(this)) &&
                    (state1!!.equals(other.state1) && state2!!.equals(other.state2) ||
                            state1!!.equals(other.state2) && state2!!.equals(other.state1) ) &&
                    areEquivalent.equals(other.areEquivalent)
            else -> false
        }
    }

    fun hasStates(state: State, otherState: State): Boolean {
        return state1!!.equals(state) && state2!!.equals(otherState) ||
                state1!!.equals(otherState) && state2!!.equals(state)
    }

    fun canEqual(other : Any?) : Boolean {
        return (other is EquivalenceRelation)
    }

    override fun hashCode() : Int {
        return 7 * ( 7 * areEquivalent.hashCode() + state2!!.hashCode()) + state1!!.hashCode()
    }

    override fun toString() : String {
        return "{" + state1!!.getStateName() + "," + state2!!.getStateName() + ": " + areEquivalent + "}"
    }
}