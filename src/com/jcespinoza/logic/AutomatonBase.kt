package com.jcespinoza.logic

import com.jcespinoza.logic.exceptions.DuplicateStateException
import com.jcespinoza.logic.exceptions.DuplicateTransitionForStateAndSymbolException
import kotlin.collections.MutableSet

/**
 * Created by jcespinoza on 7/24/2016.
 */
abstract class AutomatonBase<T : Any?> : IAutomaton<T> {
    protected var alphabet: MutableSet<Symbol<T>> = mutableSetOf()
    protected var states: MutableSet<State?> = mutableSetOf()
    protected var transitions: MutableSet<StateTransition<T>> = mutableSetOf()
    protected var acceptedStates: MutableSet<State?> = mutableSetOf()
    protected var _startState: State? = null

    override fun getAlphabet(): List<Symbol<T>> {
        return alphabet.toList()
    }

    override fun setAlphabet(alph: List<Symbol<T>>): Unit {
        val newAlphabet: MutableSet<Symbol<T>> = mutableSetOf()
        for (symbol in alph) {
            newAlphabet.add(symbol)
        }
        this.alphabet = newAlphabet
    }

    override fun getStates(): List<State> {
        val list: MutableList<State> = mutableListOf()
        for (s in states) {
            list.add(s!!)
        }
        return list
    }

    override fun getStateByName(name: String): State? {
        val searchResult = states.find { state -> state?.getStateName().equals(name) }
        return searchResult ?: null
    }

    override fun addState(st: State?): Unit {
        if (states.contains(st)) {
            throw DuplicateStateException()
        }
        states.add(st)
    }

    override fun removeState(st: State?): Unit {
        states.remove(st)
        removeTransitionsForState(st!!)
    }

    override fun removeState(name: String): Unit {
        val state = getStateByName(name)
        removeState(state)
    }

    override fun getStartState(): State {
        return _startState!!
    }

    override fun setStartState(st: State?): Unit {
        _startState = st
    }

    override fun setStartState(name: String): Unit {
        _startState = getStateByName(name)
    }


    override fun getAcceptedStates(): List<State> {
        val list: MutableList<State> = mutableListOf()
        for (s in acceptedStates) {
            list.add(s!!)
        }
        return list
    }

    override fun addAcceptedState(st: String): Unit {
        val state = getStateByName(st)
        if(state != null)
            addAcceptedState(state)
    }

    override fun addAcceptedState(st: State): Unit {
        st.isAccepted = true
        acceptedStates.add(st)
    }

    override fun removeAcceptedState(st: String): Unit {
        val state = getStateByName(st)
        removeAcceptedState(state)
    }

    override fun removeAcceptedState(st: State?): Unit {
        if (st != null) {
            st.isAccepted = false
            acceptedStates.add(st)
        }
    }

    override fun getTransitions(): List<StateTransition<T>> {
        return transitions.toList()
    }

    override fun getTransitionsForState(st: State?): List<StateTransition<T>> {
        if(transitions.isEmpty()) return transitions.toList()
        return transitions
                .filter { transition -> transition.originState != null && transition.originState!!.equals(st) }
    }

    override fun getTransitionsForStateAndSymbol(st: State?, sym: Symbol<T>): List<StateTransition<T>> {
        if(st == null) return mutableListOf()
        return getTransitionsForState(st)
            .filter {transition -> transition.triggerSymbol != null && transition.triggerSymbol!!.equals(sym) }
    }

    override fun addStateTransition(tran: StateTransition<T>): Unit {
        if(transitions.contains(tran)) throw DuplicateTransitionForStateAndSymbolException()
        transitions.add( tran )
    }

    override fun removeStateTransition(tran: StateTransition<T>?): Unit {
        if(tran != null)
            transitions.remove( tran)
    }

    private fun removeTransitionsForState(state: State): Unit {
        val transitionsToBeRemoved =
                transitions.filter { transition -> transition.originState != null && transition.originState!!.equals(state) }
        this.transitions.removeAll( transitionsToBeRemoved)
    }

    override fun acceptsSymbol(sym: Symbol<T>): Boolean
    {
        return alphabet.contains(sym)
    }

    override fun isValid(): Boolean {
        if (_startState == null) return false
        if(acceptedStates.size < 1) return false

        return true
    }
}