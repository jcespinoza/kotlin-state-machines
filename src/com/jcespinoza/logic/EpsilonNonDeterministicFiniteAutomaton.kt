package com.jcespinoza.logic

import com.jcespinoza.utils.SetUtils

/**
 * Created by jcespinoza on 7/31/2016.
 */
class EpsilonNonDeterministicFiniteAutomaton<T : Any?>(val epsilonSymbol :Symbol<T>) : AutomatonBase<T>(){

    override fun acceptsInput(symbolString: List<Symbol<T>>): Boolean {
        var currentStates : Set<State?> = mutableSetOf(_startState)
        for(symbol in symbolString) {
            if (!acceptsSymbol(symbol)) return false
            val nextStates = evaluateDestinationsForSymbol(currentStates, symbol)
            currentStates = SetUtils.Union(nextStates)
        }
        var klosedStates : MutableSet<State?> = mutableSetOf()
        for(st in currentStates){
            klosedStates = SetUtils.Union(klosedStates, evaluateKleeneClosure(st)) as MutableSet<State?>
        }
        currentStates = SetUtils.Union(currentStates, klosedStates)
        return SetUtils.Intersection(currentStates, acceptedStates).isNotEmpty()
    }

    fun evaluateDestinationsForSymbol(stateList: Set<State?>, symbol: Symbol<T>) : List<Set<State?>> {
        val listOfSets : MutableList<Set<State?>> = mutableListOf()
        var klosedStates : MutableSet<State?> = mutableSetOf()
        for(state in stateList){
            val klosure = evaluateKleeneClosure(state)
            klosedStates = SetUtils.Union(klosedStates, klosure ) as MutableSet<State?>
        }
        for(state in klosedStates){
            val destinationStates : MutableSet<State?> = mutableSetOf()
            val transitions = getTransitionsForStateAndSymbol(state, symbol)

            for(transition in transitions){
                destinationStates.add( transition.targetState )
            }

            listOfSets.add( destinationStates)
        }
        return listOfSets
    }

    fun evaluateKleeneClosure(origin : State?): Set<State?> {
        var resultStates : MutableSet<State?> = mutableSetOf(origin)

        val epsTransitions = getTransitionsForStateAndSymbol(origin, epsilonSymbol)

        for(transition in epsTransitions){
            resultStates .add( transition.targetState )
            val klosure = evaluateKleeneClosure(transition.targetState)
            resultStates = SetUtils.Union(resultStates,klosure) as MutableSet<State?>
        }

        return resultStates
    }

    override fun acceptsSymbol(sym: Symbol<T>): Boolean {
        return alphabet.contains(sym) || sym.equals(epsilonSymbol)
    }

    override fun addSymbolToAlphabet(sym: Symbol<T>): Unit {
        if(!sym.equals(epsilonSymbol)) alphabet .add( sym )
    }
}