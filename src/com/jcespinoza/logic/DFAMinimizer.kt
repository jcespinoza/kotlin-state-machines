package com.jcespinoza.logic

import com.jcespinoza.logic.common.AutomatonValidator
import com.jcespinoza.logic.exceptions.UnsupportedConversionException

/**
 * Created by jcespinoza on 8/24/2016.
 */
object DFAMinimizer {

    private fun getRestOfStatesNotProcessedYet(state: State, originalStates: List<State>, knownRelationships: MutableSet<EquivalenceRelation>)  : MutableSet<State>{
        val stateSet : MutableSet<State> = mutableSetOf()

        for(os in originalStates){
            if(!os.equals(state)){
                stateSet.add(os)
            }
        }
        return stateSet
    }

    private fun <T: Any?>getEquivalence(state: State, otherState: State, originalDFA: DeterministicFiniteAutomaton<T>, knownRelationships: MutableSet<EquivalenceRelation>) : EquivalenceRelation{
        val equivalence = EquivalenceRelation(state, otherState, false)

        val existingRelation = knownRelationships.find {r -> r.hasStates(state,otherState)}
        if(existingRelation != null) return existingRelation

        if(state.equals(otherState)) {
            equivalence.areEquivalent = true
            return equivalence
        }
        if(state.isAccepted && !otherState.isAccepted ||
                otherState.isAccepted && !state.isAccepted){
            return equivalence
        }
        equivalence.areEquivalent = true
        for(symbol in originalDFA.getAlphabet()){
            val transition = originalDFA.getTransitionForState(state, symbol)
            val otherTransition = originalDFA.getTransitionForState(otherState, symbol)
            if(transition == null && otherTransition != null ||
                    transition != null && otherTransition == null){
                equivalence.areEquivalent = false
                return equivalence
            }
            if(transition != null && otherTransition != null) {
                val destinationEquivalence = getEquivalence(transition.targetState!!, otherTransition.targetState!!, originalDFA, knownRelationships)
                if(destinationEquivalence.state1 != destinationEquivalence.state2)
                    knownRelationships.add(destinationEquivalence)
                if (!destinationEquivalence.areEquivalent) {
                    equivalence.areEquivalent = false
                    return equivalence
                }
            }
        }

        return equivalence
    }

    private fun getSetOfMergedStates(knownRelationships: MutableSet<EquivalenceRelation>) : Set<StateGroup>{
        val mergedStates : MutableSet<StateGroup> = mutableSetOf()

        knownRelationships.forEach {rl ->
            if(rl.areEquivalent){
                val pair : List<State> = listOf(rl.state1!!, rl.state2!!)
                val stateGroup = StateGroup.CreateGroup(pair)
                mergedStates.add( stateGroup)
            }
        }
        return mergedStates
    }

    private fun <T: Any?> setStartStateToNewAutomaton(resultingDFA: DeterministicFiniteAutomaton<T>, originalDFA: DeterministicFiniteAutomaton<T>, mergedStates: Set<StateGroup> ){
        val originalStartState = originalDFA.getStartState()
        val mergedStartState = mergedStates.find {s -> s.hasState(originalStartState) }
        if(mergedStartState != null) {
            resultingDFA.addState(mergedStartState)
            resultingDFA.setStartState(mergedStartState)
        }
        val startStateGroup = StateGroup.CreateGroup(originalStartState)
        resultingDFA.addState(startStateGroup)
        resultingDFA.setStartState(startStateGroup)
    }

    private fun <T: Any?> addAllStatesToResultingAutomaton(originalDFA: DeterministicFiniteAutomaton<T>, resultingDFA: DeterministicFiniteAutomaton<T>, mergedStates: Set<StateGroup>) {
        //Adding the states from the original DFA
        for(os in originalDFA.getStates()){
            val isStartState = originalDFA.getStartState().equals(os)
            if(!isStartState){
                val merged = mergedStates.find {ms -> ms.hasState(os)}
                if( merged  == null){
                    val stateGroup = StateGroup.CreateGroup(os)
                    resultingDFA.addState(stateGroup)
                }
            }
        }
        //Adding the new merged states
        for(ms in mergedStates){
            val isStartState = resultingDFA.getStartState().equals(ms)
            if(!isStartState){
                resultingDFA.addState(ms)
            }
        }
    }

    private fun <T: Any?> setAcceptingStatesOnNewAutomaton(originalDFA: DeterministicFiniteAutomaton<T>, resultingDFA: DeterministicFiniteAutomaton<T>) {
        for(acceptedState in originalDFA.getAcceptedStates()){
            resultingDFA.getStates().forEach { st ->
                if ((st as StateGroup).hasState(acceptedState)) {
                    st.isAccepted = true
                    resultingDFA.addAcceptedState(st)
                }
            }
        }
    }

    private fun <T: Any?> createTransitionsForTheNewAutomaton(originalDFA: DeterministicFiniteAutomaton<T>, resultingDFA: DeterministicFiniteAutomaton<T>) {
        for(oTransition in originalDFA.getTransitions()){
            val originState = resultingDFA.getStates().find {s -> (s as StateGroup).hasState(oTransition.originState!!)}

            if(originState != null){
                val destinationState = resultingDFA.getStates().find {s -> (s as StateGroup).hasState(oTransition.targetState!!)}

                if(destinationState != null){
                    val newTransition = StateTransition<T>()
                    newTransition.originState = originState
                    newTransition.targetState = destinationState
                    newTransition.triggerSymbol = oTransition.triggerSymbol
                    if(!resultingDFA.getTransitions().contains(newTransition)) {
                        resultingDFA.addStateTransition(newTransition)
                    }
                }
            }
        }
    }

    private fun <T: Any?> constructAutomaton(originalDFA: DeterministicFiniteAutomaton<T>, resultingDFA: DeterministicFiniteAutomaton<T>, mergedStates: Set<StateGroup>){
        resultingDFA.setAlphabet( originalDFA.getAlphabet() )
        setStartStateToNewAutomaton(resultingDFA, originalDFA, mergedStates)
        addAllStatesToResultingAutomaton(originalDFA, resultingDFA, mergedStates)
        setAcceptingStatesOnNewAutomaton(originalDFA, resultingDFA)
        createTransitionsForTheNewAutomaton(originalDFA, resultingDFA)
    }

    private fun <T: Any?> applyTransformation(originalDFA: DeterministicFiniteAutomaton<T>, resultingDFA: DeterministicFiniteAutomaton<T>){
        val knownRelationships : MutableSet<EquivalenceRelation> = mutableSetOf()

        for(state in originalDFA.getStates()){
            val restOfStates = getRestOfStatesNotProcessedYet(state, originalDFA.getStates(), knownRelationships)
            for(otherState in restOfStates){
                val relation = getEquivalence(state, otherState, originalDFA, knownRelationships)
                if(relation.state1 != relation.state2) {
                    knownRelationships.add(relation)
                }
            }
        }

        val mergedStates = getSetOfMergedStates(knownRelationships)
        constructAutomaton(originalDFA, resultingDFA, mergedStates)
    }

    private fun addRecordToKnownRelationsList(stateA : State, stateB : State, knownRelationships : MutableSet<EquivalenceRelation>, value : Boolean) : Unit {
        val newRelation = EquivalenceRelation(stateA, stateB, value)
    }

    private fun <T: Any?> MinimizeDFA(originalDFA: DeterministicFiniteAutomaton<T>) : DeterministicFiniteAutomaton<T> {
        AutomatonValidator.ThrowIfAutomatonIsInvalid(originalDFA)

        val resultingDFA = DeterministicFiniteAutomaton<T>()

        applyTransformation(originalDFA, resultingDFA)

        return resultingDFA
    }

    fun <T: Any?> Minimize(automaton : IAutomaton<T>) : DeterministicFiniteAutomaton<T> {
        return when (automaton) {
            is DeterministicFiniteAutomaton<T> -> MinimizeDFA(automaton)
            is NonDeterministicFiniteAutomaton<T> -> MinimizeDFA(AutomataConverter.ConvertToDFA(automaton))
            else -> throw UnsupportedConversionException()
        }
    }
}