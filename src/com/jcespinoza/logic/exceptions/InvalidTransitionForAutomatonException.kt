package com.jcespinoza.logic.exceptions

/**
 * Created by jcespinoza on 8/2/2016.
 */
class InvalidTransitionForAutomatonException(message : String = "This automaton type does not allow this kind of transition") : IllegalArgumentException(message)