package com.jcespinoza.logic.exceptions

/**
 * Created by jcespinoza on 8/2/2016.
 */
class UnsupportedConversionException(message : String = "This conversion is not yet supported. Although they failed, the devs made great effort to implement it and deserve your forgiveness") : IllegalArgumentException(message)