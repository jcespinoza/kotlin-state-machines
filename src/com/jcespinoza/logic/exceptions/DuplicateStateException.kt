package com.jcespinoza.logic.exceptions

/**
 * Created by jcespinoza on 7/24/2016.
 */
class DuplicateStateException(message : String = "The given state already exists in the automaton") : IllegalArgumentException(message)