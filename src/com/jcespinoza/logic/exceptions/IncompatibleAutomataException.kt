package com.jcespinoza.logic.exceptions

/**
 * Created by jcespinoza on 8/2/2016.
 */
class IncompatibleAutomataException(message : String = "The given automata are not compatible") : IllegalArgumentException(message)