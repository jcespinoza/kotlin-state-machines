package com.jcespinoza.logic.exceptions

/**
 * Created by jcespinoza on 7/24/2016.
 */
class DuplicateTransitionForStateAndSymbolException(message : String = "There is already a transition with this symbol from this state") : IllegalArgumentException(message)