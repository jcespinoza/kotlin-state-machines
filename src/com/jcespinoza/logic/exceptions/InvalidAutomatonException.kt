package com.jcespinoza.logic.exceptions

/**
 * Created by jcespinoza on 8/2/2016.
 */
class InvalidAutomatonException(message : String = "The given automaton is not complete") : IllegalArgumentException(message)