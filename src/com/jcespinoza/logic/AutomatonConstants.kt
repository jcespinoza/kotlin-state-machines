package com.jcespinoza.logic

/**
 * Created by jcespinoza on 8/1/2016.
 */
object AutomatonConstants{
    var epsilon : Char = '\u03B5'
    final val DFA = "DFA"
    final val NFA = "NFA"
    final val eNFA = "eNFA"
    final val APDA = "APDA"
}