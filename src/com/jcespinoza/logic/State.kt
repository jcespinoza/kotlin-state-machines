package com.jcespinoza.logic

/**
 * Created by jcespinoza on 7/24/2016.
 */
open class State constructor(name: String = "", accepted : Boolean = false) : Comparable<State>
{
    constructor(name: String) : this(name, false){

    }

    private val stateName : String = name
    var isAccepted : Boolean = accepted

    open fun getStateName() : String {
        return stateName
    }

    override fun equals(other : Any?) : Boolean  {
        when (other) {
            is State -> {
            val that = other
            return (that.canEqual( this )) &&
                    getStateName().equals(that.getStateName())
            }
        }
        return false
    }

    override fun hashCode() : Int
    {
        return 7 * (7 * isAccepted.hashCode()) + getStateName().hashCode()
    }

    override fun toString() : String {
        return "{name = " + getStateName() + ", accepted = " + (if (isAccepted) "true" else "false") + "}"
    }

    private fun canEqual(other: Any): Boolean {
        return other is State
    }

    override fun compareTo(other: State): Int {
        return getStateName().compareTo(other.getStateName())
    }
}