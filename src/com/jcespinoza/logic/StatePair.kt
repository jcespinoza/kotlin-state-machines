package com.jcespinoza.logic

/**
 * Created by jcespinoza on 8/24/2016.
 */
class StatePair (var stateA : State? = null, var stateB : State? = null) {

    fun firstStateIs(state : State?) : Boolean
    {
        return statesEqual(stateA, state)
    }

    fun secondStateIs(state : State?) : Boolean
    {
        return statesEqual(stateB, state)
    }

    private fun statesEqual(state1 : State?, state2 : State?) : Boolean
    {
        return state1 == null && state2 == null ||
                state1 != null && state1.equals(state2)
    }

    companion object {
        fun CreatePair(state1: State, state2: State): StatePair {
            val statePair = StatePair()
            statePair.stateA = state1
            statePair.stateB = state2

            return statePair
        }
    }
}