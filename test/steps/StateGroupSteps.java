package steps;

import com.jcespinoza.logic.State;
import com.jcespinoza.logic.StateGroup;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import spec.SpecContext;
import spec.SpecUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;


/**
 * Created by jcespinoza on 8/2/2016.
 */
public class StateGroupSteps {
    @Given("^an empty list of states$")
    public void anEmptyListOfStates() throws Throwable {
        ArrayList<State> stateList = new ArrayList<>();
        SpecContext.getInstance().context.put("stateList", stateList);
    }

    @And("^I add a state \"([^\"]*)\" to the list$")
    public void iAddAStateToTheList(String stateName) throws Throwable {
        ArrayList<State> stateList = (ArrayList<State>)SpecContext.getInstance().context.get("stateList");
        State newState = new State(stateName);
        stateList.add( newState );
    }

    @When("^I create a StateGroup$")
    public void iCreateAStateGroup() throws Throwable {
        ArrayList<State> stateList = (ArrayList<State>)SpecContext.getInstance().context.get("stateList");
        StateGroup resultingStateGroup = StateGroup.Group.CreateGroup(stateList);
        SpecContext.getInstance().context.put("resultingStateGroup", resultingStateGroup);
    }

    @Then("^the resulting StateGroup should have \"([^\"]*)\" states$")
    public void theResultingStateGroupShouldHaveStates(String statesCount) throws Throwable {
        StateGroup resultingStateGroup = (StateGroup)SpecContext.getInstance().context.get("resultingStateGroup");
        int numberOfStates = SpecUtils.InterpretDecimalName(statesCount);
        Assert.assertTrue("Number of states is not correct", numberOfStates == resultingStateGroup.getInnerStates().size());
    }

    @And("^the name of the resulting StateGroup should be \"([^\"]*)\"$")
    public void theNameOfTheResultingStateGroupShouldBe(String stateName) throws Throwable {
        StateGroup resultingStateGroup = (StateGroup)SpecContext.getInstance().context.get("resultingStateGroup");
        Assert.assertTrue("The name of the resulting StateGroup is incorrect", resultingStateGroup.getStateName().equals(stateName));
    }

    @Given("^an empty list of StateGroups$")
    public void anEmptyListOfStateGroups() throws Throwable {
        ArrayList<StateGroup> stateGroupList = new ArrayList<>();
        SpecContext.getInstance().context.put("stateGroupList", stateGroupList);
    }

    @And("^I create a StateGroup and add it to the list$")
    public void iCreateAStateGroupAndAddItToTheList() throws Throwable {
        ArrayList<State> stateList = (ArrayList<State>)SpecContext.getInstance().context.get("stateList");
        StateGroup resultingStateGroup = StateGroup.Group.CreateGroup(stateList);
        ArrayList<StateGroup> stateGroupList = (ArrayList<StateGroup>)SpecContext.getInstance().context.get("stateGroupList");

        stateGroupList.add( resultingStateGroup );
    }

    @When("^I create a StateGroup out the StateGroups$")
    public void iCreateAStateGroupOutTheStateGroups() throws Throwable {
        ArrayList<StateGroup> stateGroupList = (ArrayList<StateGroup>)SpecContext.getInstance().context.get("stateGroupList");
        StateGroup resultingStateGroup = StateGroup.Group.CreateGroup(stateGroupList);
        SpecContext.getInstance().context.put("resultingStateGroup", resultingStateGroup);
    }

    @When("^I compare the first two StateGroups in the list$")
    public void iCompareTheFirstTwoStateGroupsInTheList() throws Throwable {
        ArrayList<StateGroup> stateGroupList = (ArrayList<StateGroup>)SpecContext.getInstance().context.get("stateGroupList");
        StateGroup firstStateGroup = stateGroupList.get(0);
        StateGroup secondStateGroup = stateGroupList.get(1);
        Boolean stateGroupComparison = firstStateGroup.equals(secondStateGroup);

        SpecContext.getInstance().context.put("stateGroupComparison", stateGroupComparison);
    }

    @Then("^the StateGroups should equal$")
    public void theStateGroupsShouldEqual() throws Throwable {
        Boolean stateGroupComparison = (Boolean)SpecContext.getInstance().context.get("stateGroupComparison");
        Assert.assertTrue( "StateGroups are not equal", stateGroupComparison);
    }

    @When("^I compare the first two StateGroups' names in the list$")
    public void iCompareTheFirstTwoStateGroupsNamesInTheList() throws Throwable {
        ArrayList<StateGroup> stateGroupList = (ArrayList<StateGroup>)SpecContext.getInstance().context.get("stateGroupList");
        StateGroup firstStateGroup = stateGroupList.get(0);
        StateGroup secondStateGroup = stateGroupList.get(1);
        Boolean stateGroupNameComparison = firstStateGroup.getStateName().equals(secondStateGroup.getStateName());
        SpecContext.getInstance().context.put("stateGroupNameComparison", stateGroupNameComparison);
    }

    @Then("^the StateGroups should have the same name$")
    public void theStateGroupsShouldHaveTheSameName() throws Throwable {
        Boolean stateGroupNameComparison = (Boolean)SpecContext.getInstance().context.get("stateGroupNameComparison");
        Assert.assertTrue("StateGroup names are not equal", stateGroupNameComparison);
    }
}
