package steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.jcespinoza.logic.State;
import com.jcespinoza.logic.StateTransition;
import com.jcespinoza.logic.Symbol;
import org.junit.Assert;
import spec.SpecContext;

import java.util.List;

/**
 * Created by jcespinoza on 7/24/2016.
 */
@SuppressWarnings("unchecked")
public class DFASteps {
    @When("^I ask it for a transition with the symbol \"([^\"]*)\" for the state \"([^\"]*)\"$")
    public void iAskItForATransitionWithTheSymbolForTheState(String symbolName, String stateName) throws Throwable {
        State state = new State(stateName);
        Symbol<Character> symbol = new Symbol<>(symbolName.toCharArray()[0]);
        List<StateTransition<Character>> transitions = SpecContext.Automaton.getTransitionsForStateAndSymbol(state, symbol);
        SpecContext.getInstance().context.put("transitions",transitions);
    }

    @Then("^it should return only one transition with \"([^\"]*)\" to \"([^\"]*)\"$")
    public void itShouldReturnOnlyOneTransitionWithTo(String originName, String destinationName) throws Throwable {
        List<StateTransition<Character>> transitions =   SpecContext.Automaton.getTransitions();
        StateTransition<Character> transition = transitions.get(0);

        Assert.assertTrue("The number of transitions is illegal", transitions.size() == 1);
        Assert.assertTrue("Origin State names differ", transition.getOriginState().getStateName().equals(originName) );
        Assert.assertTrue("Destination State names differ", transition.getTargetState().getStateName().equals(destinationName) );
    }

    @Then("^it should return no states$")
    public void itShouldReturnNoStates() throws Throwable {
        List<StateTransition<Character>> transitions = (List<StateTransition<Character>>) SpecContext.getInstance().context.get("transitions");

        Assert.assertTrue ("Returned wrong number of transitions", transitions.isEmpty());
    }

    @When("^I try to add a transition from state \"([^\"]*)\" to state \"([^\"]*)\" with the symbol \"([^\"]*)\"$")
    public void iTryToAddATransitionFromStateToStateWithTheSymbol(String originName, String destinationName, String symbolName) throws Throwable {
        State originState = new State(originName);
        State destinationState = new State(destinationName);
        Symbol<Character> symbol = new Symbol<>(symbolName.toCharArray()[0]);
        StateTransition<Character> transition = new StateTransition<>(originState, destinationState, symbol);

        try {
            SpecContext.Automaton.addStateTransition(transition);
        }catch(Exception e){

        }
    }

    @Then("^it should not have two outputs from \"([^\"]*)\" with the symbol \"([^\"]*)\"$")
    public void itShouldNotHaveTwoOutputsFromWithTheSymbol(String stateName, String symbolName) throws Throwable {
        State state = SpecContext.Automaton.getStateByName(stateName);
        Symbol<Character> symbol = new Symbol<>(symbolName.toCharArray()[0]);
        List< StateTransition<Character> > transitions = SpecContext.Automaton.getTransitionsForStateAndSymbol(state, symbol);

        Assert.assertTrue("Multiple transitions for the same symbol are not allowed", transitions.size() < 2);
    }
}
