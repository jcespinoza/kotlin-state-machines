package steps;

import com.jcespinoza.logic.AutomataConverter;
import com.jcespinoza.logic.DFAMinimizer;
import com.jcespinoza.logic.DeterministicFiniteAutomaton;
import com.jcespinoza.logic.IAutomaton;
import com.jcespinoza.logic.exceptions.InvalidAutomatonException;
import cucumber.api.PendingException;
import cucumber.api.java.en.When;
import spec.SpecContext;

/**
 * Created by jcespinoza on 8/24/2016.
 */
public class MinimizationSteps {
    @When("^I try to minimize it$")
    public void iTryToMinimizeIt() throws Throwable {
        IAutomaton<Character> automaton = SpecContext.Automaton;

        try {
            DeterministicFiniteAutomaton<Character> resultingAutomaton;
            resultingAutomaton = DFAMinimizer.INSTANCE.Minimize(automaton);
            SpecContext.getInstance ().context.put("resultingAutomaton",resultingAutomaton);
        } catch(InvalidAutomatonException e) {
            SpecContext.getInstance().context.put("invalidAutomatonException", e);
        }
    }
}
