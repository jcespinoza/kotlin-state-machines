package steps;

import com.jcespinoza.gui.model.AutomatonModel;
import com.jcespinoza.gui.model.StateModel;
import com.jcespinoza.gui.model.TransitionModel;
import com.jcespinoza.io.AutomatonDeserializer;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import spec.SpecContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jcespinoza on 8/3/2016.
 */
public class DeserializationSteps {
    @Given("^a file \"([^\"]*)\" with the following contents$")
    public void aFileWithTheFollowingContents(String fileName, String contents) throws Throwable {
        String noCarriage = contents.replace("\r", "");
        SpecContext.getInstance().context.put(fileName, noCarriage);
    }

    @When("^I deserialize the file \"([^\"]*)\"$")
    public void iDeserializeTheFile(String fileName) throws Throwable {
        String fileContents = (String) SpecContext.getInstance().context.get(fileName);
        ArrayList<String> lines = new ArrayList<>();
        Collections.addAll(lines, fileContents.split("\n"));

        Object resultingModel = AutomatonDeserializer.INSTANCE.buildFromLines(lines);

        SpecContext.getInstance().context.put("resultingModel", resultingModel);
    }

    @Then("^the resulting model should have these properties$")
    public void theResultingModelShouldHaveThisProperties(List<AutomatonModel> models) throws Throwable {
        AutomatonModel expectedModel = models.get(0);

        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        Assert.assertEquals("Names are not equal", expectedModel.getName(), resultingModel.getName());
        Assert.assertEquals("Epsilon symbols not equal", expectedModel.getEpsilon(), resultingModel.getEpsilon());
        Assert.assertEquals("Types are not equal", expectedModel.getType(), resultingModel.getType());
    }

    @And("^the resulting model should have no states$")
    public void theResultingModelShouldHaveNoStates() throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        Assert.assertEquals("State count is not the same", resultingModel.getStateSet().size(), 0);
    }

    @And("^the resulting model should have these states$")
    public void theResultingModelShouldHaveThisStates(List<StateModel> stateModels) throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        Assert.assertEquals("State count is not the same", resultingModel.getStateSet().size(), stateModels.size());

        for (int i = 0; i < stateModels.size(); i++) {
            StateModel expected = stateModels.get(i);
            StateModel actual = resultingModel.getStateSet().get(i);
            Assert.assertEquals("State names do not mach", actual.getName(), expected.getName());
            Assert.assertEquals("Acceptedness does not mach", actual.getAccepted(), expected.getAccepted());
            Assert.assertEquals("X coordinates do not mach", actual.getX(), expected.getX(), 0.01);
            Assert.assertEquals("Y coordinates do not mach", actual.getY(), expected.getY(), 0.01);
        }
    }

    @And("^the resulting model should have no initial state$")
    public void theResultingModelShouldHaveNoInitialState() throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");
        Assert.assertNull(resultingModel.getInitialState());
    }

    @And("^the resulting model should have the following initial state$")
    public void theResultingModelShouldHaveTheFollowingInitialState(List<StateModel> stateModels) throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        StateModel expected = stateModels.get(0);

        StateModel actual = resultingModel.getInitialState();

        Assert.assertNotNull(actual);
        Assert.assertEquals("State names do not mach", actual.getName(), expected.getName());
        Assert.assertEquals("Acceptedness does not mach", actual.getAccepted(), expected.getAccepted());
        Assert.assertEquals("X coordinates do not mach", actual.getX(), expected.getX(), 0.01);
        Assert.assertEquals("Y coordinates do not mach", actual.getY(), expected.getY(), 0.01);
    }

    @And("^the resulting model should have no accepted states$")
    public void theResultingModelShouldHaveNoAcceptedStates() throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        Assert.assertEquals("State count is not the same", resultingModel.getAcceptedStates().size(), 0);
    }

    @And("^the resulting model should have the following accepted states$")
    public void theResultingModelShouldHaveTheFollowingAcceptedStates(List<StateModel> stateModels) throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        Assert.assertEquals("State count is not the same", resultingModel.getAcceptedStates().size(), stateModels.size());

        for (int i = 0; i < stateModels.size(); i++) {
            StateModel expected = stateModels.get(i);
            StateModel actual = resultingModel.getAcceptedStates().get(i);
            Assert.assertEquals("State names do not mach", actual.getName(), expected.getName());
            Assert.assertEquals("Acceptedness does not mach", actual.getAccepted(), expected.getAccepted());
            Assert.assertEquals("X coordinates do not mach", actual.getX(), expected.getX(), 0.01);
            Assert.assertEquals("Y coordinates do not mach", actual.getY(), expected.getY(), 0.01);
        }
    }

    @And("^the resulting model should have no alphabet$")
    public void theResultingModelShouldHaveNoAlphabet() throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        Assert.assertEquals("Symbol count is not the same", resultingModel.getAlphabet().size(), 0);
    }

    @And("^the resulting model should have the following alphabet$")
    public void theResultingModelShouldHaveTheFollowingAlphabet(List<String> alphabet) throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        Assert.assertEquals("Symbol count is not the same", resultingModel.getAlphabet().size(), alphabet.size());

        for (int i = 0; i < alphabet.size(); i++) {
            String expected = alphabet.get(i);
            String actual = resultingModel.getAlphabet().get(i);
            Assert.assertEquals("Symbol does not match", actual, expected);
        }
    }

    @And("^the resulting model should have no transitions$")
    public void theResultingModelShouldHaveNoTransitions() throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        Assert.assertEquals("Transition count is not the same", resultingModel.getTransitions().size(), 0);
    }

    @And("^the resulting model should have the following transitions$")
    public void theResultingModelShouldHaveTheFollowingTransitions(List<TransitionModel> transitionModels) throws Throwable {
        AutomatonModel resultingModel = (AutomatonModel) SpecContext.getInstance().context.get("resultingModel");

        Assert.assertEquals("Transition count is not the same", resultingModel.getTransitions().size(), transitionModels.size());

        for (int i = 0; i < transitionModels.size(); i++) {
            TransitionModel expected = transitionModels.get(i);
            TransitionModel actual = resultingModel.getTransitions().get(i);
            Assert.assertEquals("Source name does not mach", actual.getSource(), expected.getSource());
            Assert.assertEquals("Target name does not mach", actual.getTarget(), expected.getTarget());
            Assert.assertEquals("Symbol does not mach", actual.getSymbol(), expected.getSymbol());
        }
    }
}