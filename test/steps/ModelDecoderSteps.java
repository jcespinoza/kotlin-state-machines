package steps;

import com.jcespinoza.gui.JGraphHelper;
import com.jcespinoza.gui.model.AutomatonModel;
import com.jcespinoza.gui.model.ModelDecoder;
import com.jcespinoza.logic.IAutomaton;
import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.junit.Assert;
import spec.SpecContext;
import spec.SpecUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcespinoza on 8/7/2016.
 */
public class ModelDecoderSteps {
    @When("^I decode the model \"([^\"]*)\" to an automaton with graph \"([^\"]*)\"$")
    public void iDecodeTheModelToAnAutomatonWithGraph(String automatonName, String graphName) throws Throwable {
        AutomatonModel model = (AutomatonModel)(SpecContext.getInstance().context.get(automatonName));
        mxGraph graph = (mxGraph)SpecContext.getInstance().context.get(graphName);

        IAutomaton<Character> resultingAutomaton = ModelDecoder.INSTANCE.decodeModel(model, graph);

        SpecContext.getInstance().context.put("resultingAutomaton", resultingAutomaton);
    }

    @And("^the graph \"([^\"]*)\" should have this vertices$")
    public void theGraphShouldHaveThisVertices(String graphName, List<String> states) throws Throwable {
        mxGraph graph = (mxGraph)SpecContext.getInstance().context.get(graphName);

        List<mxCell> vertices = JGraphHelper.INSTANCE.getAllVertices(graph);

        for (mxCell vertex : vertices){
            Assert.assertTrue("State was not expected", states.contains(vertex.getValue()));
        }
    }

    @And("^the graph \"([^\"]*)\" should have \"([^\"]*)\" states$")
    public void theGraphShouldHaveStates(String graphName, String stateCount) throws Throwable {
        mxGraph graph = (mxGraph)SpecContext.getInstance().context.get(graphName);
        List<mxCell> vertices = JGraphHelper.INSTANCE.getAllVertices(graph);
        int expectedCount = SpecUtils.InterpretDecimalName(stateCount);
        Assert.assertEquals("State count does not match", expectedCount,vertices.size());
    }
}
