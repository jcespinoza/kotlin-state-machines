package steps;

import com.jcespinoza.gui.model.AutomatonModel;
import com.jcespinoza.gui.model.ModelGenerator;
import com.jcespinoza.gui.model.StateModel;
import com.jcespinoza.logic.IAutomaton;
import com.mxgraph.view.mxGraph;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import spec.SpecContext;

import java.util.List;

/**
 * Created by jcespinoza on 8/4/2016.
 */
public class ModelGeneratorSteps {
    @And("^an existing graph \"([^\"]*)\"$")
    public void anExistingGraph(String graphName) throws Throwable {
        mxGraph graph = new mxGraph();
        SpecContext.getInstance().context.put(graphName, graph);
    }

    @And("^the following vertices in the graph \"([^\"]*)\"$")
    public void theFollowingVerticesInTheGraph(String graphName, List<StateModel> stateModels) throws Throwable {
        mxGraph graph = (mxGraph)SpecContext.getInstance().context.get(graphName);

        for(StateModel state : stateModels){
            graph.insertVertex(graph.getDefaultParent(), null, state.getName(), state.getX(), state.getY(), 50.0, 50);
        }
    }

    @When("^I generate an automaton model \"([^\"]*)\"$")
    public void iGenerateAnAutomatonModel(String graphName) throws Throwable {
        mxGraph graph = (mxGraph)SpecContext.getInstance().context.get(graphName);
        IAutomaton<Character> automaton = SpecContext.Automaton;

        AutomatonModel resultingModel = ModelGenerator.INSTANCE.GenerateModel(automaton, graph, graphName);

        SpecContext.getInstance().context.put("resultingModel", resultingModel);
    }
}
