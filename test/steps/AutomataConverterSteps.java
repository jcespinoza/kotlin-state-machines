package steps;

import com.jcespinoza.logic.*;
import com.jcespinoza.logic.exceptions.InvalidAutomatonException;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import spec.SpecContext;
import spec.SpecUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcespinoza on 8/2/2016.
 */
public class AutomataConverterSteps {
    @When("^I try to convert it to a \"([^\"]*)\"$")
    public void iTryToConvertItToA(String automatonType) throws Throwable {
        IAutomaton<Character> originalAutomaton = SpecContext.Automaton;
        try {
            IAutomaton<Character> resultingAutomaton = null;
            switch(automatonType) {
                case "DFA" : resultingAutomaton = AutomataConverter.INSTANCE.ConvertToDFA(originalAutomaton);
                default:
            }
            SpecContext.getInstance ().context.put("resultingAutomaton",resultingAutomaton);
        } catch(InvalidAutomatonException e) {
            SpecContext.getInstance().context.put("invalidAutomatonException", e);
        }
    }

    @Then("^it should throw an error since it is invalid$")
    public void itShouldThrowAnErrorSinceItIsInvalid() throws Throwable {
        Object invalidAutomatonException = SpecContext.getInstance().context.getOrDefault("invalidAutomatonException", null);
        Assert.assertTrue("Exception wasn't trown", invalidAutomatonException != null);
    }

    @Then("^it should return the same automaton$")
    public void itShouldReturnTheSameAutomaton() throws Throwable {
        IAutomaton<Character> resultingAutomaton = (IAutomaton<Character>) SpecContext.getInstance().context.get("resultingAutomaton");
        Assert.assertTrue("It didn't return the same automaton", resultingAutomaton == SpecContext.Automaton);
    }

    @Then("^it should have a start state \"([^\"]*)\"$")
    public void itShouldHaveAStartState(String stateName) throws Throwable {
        IAutomaton<Character> resultingAutomaton = (IAutomaton<Character>) SpecContext.getInstance().context.get("resultingAutomaton");
        Assert.assertTrue("The Start State is not correct", resultingAutomaton.getStartState().getStateName().equals(stateName));
    }

    @And("^it should have a state \"([^\"]*)\"$")
    public void itShouldHaveAState(String stateName) throws Throwable {
        IAutomaton<Character> resultingAutomaton = (IAutomaton<Character>) SpecContext.getInstance().context.get("resultingAutomaton");
        State state = resultingAutomaton.getStateByName(stateName);
        Assert.assertTrue("State was not found", state != null);
    }


    @And("^it should have a final state \"([^\"]*)\"$")
    public void itShouldHaveAFinalState(String stateName) throws Throwable {
        IAutomaton<Character> resultingAutomaton = (IAutomaton<Character>) SpecContext.getInstance().context.get("resultingAutomaton");
        State state = resultingAutomaton.getStateByName(stateName);
        Assert.assertTrue("State was not found", state != null);
        Assert.assertTrue("State was not found", state.isAccepted());
    }

    @And("^it should have a transition from \"([^\"]*)\" to \"([^\"]*)\" with \"([^\"]*)\"$")
    public void itShouldHaveATransitionFromToWith(String originName, String destinationName, String symbolName) throws Throwable {
        IAutomaton<Character> resultingAutomaton = (IAutomaton<Character>) SpecContext.getInstance().context.get("resultingAutomaton");
        Symbol<Character> symbol = new Symbol<Character>(symbolName.toCharArray()[0]);
        ArrayList<StateTransition<Character>> transitions = new ArrayList<>();
        for (StateTransition<Character> t : resultingAutomaton.getTransitions()) {
            if((t.getTriggerSymbol().equals( symbol)) &&
            t.getOriginState().getStateName().equals(originName) && t.getTargetState().getStateName().equals(destinationName)) {
                transitions.add(t);
            }
        }
        Assert.assertTrue("Transition was not found", transitions.size() == 1);
    }

    @Then("^the resulting automaton should have \"([^\"]*)\" states and \"([^\"]*)\" transitions$")
    public void theResultingAutomatonShouldHaveStatesAndTransitions(String statesCount, String transitionsCount) throws Throwable {
        IAutomaton<Character> resultingAutomaton = (IAutomaton<Character>) SpecContext.getInstance().context.get("resultingAutomaton");
        int expectedStates = SpecUtils.InterpretDecimalName(statesCount);
        int expectedTransitions = SpecUtils.InterpretDecimalName(transitionsCount);
        int numberOfStates = resultingAutomaton.getStates().size();
        int numberOfTransitions = resultingAutomaton.getTransitions().size();

        Assert.assertTrue("Transition count is incorrect", expectedTransitions == numberOfTransitions);
        Assert.assertTrue("Number of states is incorrect", expectedStates == numberOfStates);
    }

    @Then("^both the original and resulting automaton should accept the string \"([^\"]*)\"$")
    public void bothTheOriginalAndResultingAutomatonShouldAcceptTheString(String testString) throws Throwable {
        IAutomaton<Character> resultingAutomaton = (IAutomaton<Character>) SpecContext.getInstance().context.get("resultingAutomaton");
        List<Symbol<Character>> symbols = new ArrayList<>();
        for(char character : testString.toCharArray()){
            Symbol symbol = new Symbol<>(character);
            symbols.add( symbol );
        }
        Assert.assertTrue("The resulting automaton does not accept the string", resultingAutomaton.acceptsInput(symbols));
        Assert.assertTrue("The original automaton does not accept the string", SpecContext.Automaton.acceptsInput(symbols));
    }

    @Then("^both the original and resulting automaton should reject the string \"([^\"]*)\"$")
    public void bothTheOriginalAndResultingAutomatonShouldRejectTheString(String testString) throws Throwable {
        IAutomaton<Character> resultingAutomaton = (IAutomaton<Character>) SpecContext.getInstance().context.get("resultingAutomaton");
        List<Symbol<Character>> symbols = new ArrayList<>();
        for(char character : testString.toCharArray()){
            Symbol symbol = new Symbol<>(character);
            symbols.add( symbol );
        }
        Assert.assertFalse("The resulting automaton does not reject the string", resultingAutomaton.acceptsInput(symbols));
        Assert.assertFalse("The original automaton does not reject the string", SpecContext.Automaton.acceptsInput(symbols));
    }

    @And("^it should have the following alphabet$")
    public void itShouldHaveTheFollowingAlphabet(List<String> symbols) throws Throwable {
        IAutomaton<Character> resultingAutomaton = (IAutomaton<Character>) SpecContext.getInstance().context.get("resultingAutomaton");
        for(String character : symbols){
            Symbol symbol = new Symbol<>(character.charAt(0));

            Assert.assertTrue("Alphabet does not contain the symbol" + character, resultingAutomaton.getAlphabet().contains(symbol));
        }
    }
}