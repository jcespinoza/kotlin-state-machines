package steps;

import com.jcespinoza.logic.EquivalenceRelation;
import com.jcespinoza.logic.State;
import com.sun.org.apache.xpath.internal.operations.Bool;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import spec.SpecContext;
import spec.SpecUtils;

import java.util.ArrayList;

/**
 * Created by jcespinoza on 8/24/2016.
 */
public class EquivalenceRelationSteps {
    @Given("^an existing two-element list$")
    public void anExistingTwoElementList() throws Throwable {
        ArrayList<Object> list = new ArrayList<>();

        SpecContext.getInstance().context.put("twoElementList", list);
    }

    @Given("^an EquivalenceRelation object$")
    public void anEquivalenceRelationObject() throws Throwable {
        EquivalenceRelation equivalenceRelation = new EquivalenceRelation(null, null, false);
        SpecContext.getInstance().context.put("equivalenceRelation", equivalenceRelation);
    }

    @And("^I set the state1 to a State \"([^\"]*)\"$")
    public void iSetTheState1ToAState(String stateName) throws Throwable {
        State state = new State(stateName);
        EquivalenceRelation equivalenceRelation = (EquivalenceRelation)SpecContext.getInstance().context.get("equivalenceRelation");
        equivalenceRelation.setState1(state);
    }

    @And("^I set the state2 to a State \"([^\"]*)\"$")
    public void iSetTheState2ToAState(String stateName) throws Throwable {
        State state = new State(stateName);
        EquivalenceRelation equivalenceRelation = (EquivalenceRelation)SpecContext.getInstance().context.get("equivalenceRelation");
        equivalenceRelation.setState2(state);
    }

    @And("^I set the areEquivalent to \"([^\"]*)\"$")
    public void iSetTheAreEquivalentTo(String booleanValue) throws Throwable {
        EquivalenceRelation equivalenceRelation = (EquivalenceRelation)SpecContext.getInstance().context.get("equivalenceRelation");
        equivalenceRelation.setAreEquivalent(SpecUtils.InterpretBoolean(booleanValue));
    }

    @And("^I add it to a two element list of objects$")
    public void iAddItToATwoElementListOfObjects() throws Throwable {
        EquivalenceRelation equivalenceRelation = (EquivalenceRelation)SpecContext.getInstance().context.get("equivalenceRelation");
        ArrayList<Object> list = (ArrayList<Object>) SpecContext.getInstance().context.get("twoElementList");
        list.add(equivalenceRelation);

    }

    @When("^I compare both EquivalenceObjects$")
    public void iCompareBothEquivalenceObjects() throws Throwable {
        ArrayList<Object> list = (ArrayList<Object>) SpecContext.getInstance().context.get("twoElementList");
        EquivalenceRelation firstObject = (EquivalenceRelation) list.get(0);
        EquivalenceRelation secondObject = (EquivalenceRelation) list.get(1);
        Boolean comparisonResult = firstObject.equals(secondObject);

        SpecContext.getInstance().context.put("comparisonResult", comparisonResult);
    }

    @Then("^the comparison result of EquivalenceObjects should be \"([^\"]*)\"$")
    public void theComparisonResultOfEquivalenceObjectsShouldBe(String arg0) throws Throwable {
        Boolean list = (Boolean) SpecContext.getInstance().context.get("comparisonResult");

    }
}
