package steps;

import com.jcespinoza.gui.model.AutomatonModel;
import com.jcespinoza.gui.model.StateModel;
import com.jcespinoza.gui.model.TransitionModel;
import com.jcespinoza.io.AutomatonSerializer;
import com.jcespinoza.logic.IAutomaton;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import spec.SpecContext;

import java.util.List;

/**
 * Created by jcespinoza on 8/3/2016.
 */
public class SerializationSteps {
    @Given("^an existing automaton model \"([^\"]*)\" with the following properties$")
    public void anExistingAutomatonModelWithTheFollowingProperties(String automatonName, List<AutomatonModel> models) throws Throwable {
        AutomatonModel model = models.get(0);
        SpecContext.getInstance().context.put(automatonName, model);
    }

    @And("^the following state is assigned as initial to \"([^\"]*)\"$")
    public void theFollowingStateIsAssignedAsInitialTo(String automatonName, List<StateModel> stateModels) throws Throwable {
        AutomatonModel model = (AutomatonModel)(SpecContext.getInstance().context.get(automatonName));
        model.setInitialState(stateModels.get(0));
    }

    @And("^the following alphabet model is assigned to \"([^\"]*)\"$")
    public void theFollowingAlphabetModelIsAssignedTo(String automatonName, List<String> alphabetModel) throws Throwable {
        AutomatonModel model = (AutomatonModel)(SpecContext.getInstance().context.get(automatonName));
        model.setAlphabet(alphabetModel);
    }

    @And("^the following states are assigned as final states for \"([^\"]*)\"$")
    public void theFollowingStatesAreAssignedAsFinalStatesFor(String automatonName, List<StateModel> states) throws Throwable {
        AutomatonModel model = (AutomatonModel)(SpecContext.getInstance().context.get(automatonName));
        model.setAcceptedStates(states);
    }

    @And("^the following state models are assigned to \"([^\"]*)\"$")
    public void theFollowingStateModelsIsAssignedTo(String automatonName, List<StateModel> stateModels) throws Throwable {
        AutomatonModel model = (AutomatonModel)(SpecContext.getInstance().context.get(automatonName));
        model.setStateSet(stateModels);
    }

    @And("^the following transition models are assigned to \"([^\"]*)\"$")
    public void theFollowingTransitionModelsIsAssignedTo(String automatonName, List<TransitionModel> transitionModels) throws Throwable {
        AutomatonModel model = (AutomatonModel)(SpecContext.getInstance().context.get(automatonName));
        model.setTransitions(transitionModels);
    }


    @When("^I serialize the automaton \"([^\"]*)\"$")
    public void iSerializeTheAutomaton(String automatonName) throws Throwable {
        AutomatonModel model = (AutomatonModel)(SpecContext.getInstance().context.get(automatonName));
        String serializedModel = AutomatonSerializer.INSTANCE.buildString(model);

        SpecContext.getInstance().context.put("serializedModel", serializedModel);
    }

    @Then("^the resulting string should be$")
    public void theResultingStringShouldBe(String expectedResult) throws Throwable {
        String serializedModel = (String)(SpecContext.getInstance().context.get("serializedModel"));
        String noCarriage = expectedResult.replace("\r","");
        Assert.assertEquals("Serialization procedure did not return the expected string", noCarriage, serializedModel);
    }
}
