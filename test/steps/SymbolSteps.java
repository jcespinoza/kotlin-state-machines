package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.jcespinoza.logic.Symbol;
import org.junit.Assert;
import spec.SpecContext;

/**
 * Created by jcespinoza on 7/24/2016.
 */
@SuppressWarnings("unchecked")
public class SymbolSteps {
    @Given("^I have one symbol named \"([^\"]*)\" with value \"([^\"]*)\"$")
    public void iHaveOneSymbolNamedWithValue(String name, String value) throws Throwable {
        Symbol<Character> symbol = new Symbol<>(value.charAt(0), false);
        SpecContext.getInstance().context.put(name, symbol);
    }

    @When("^I compare the symbols \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iCompareTheSymbolsAnd(String name1, String name2) throws Throwable {
        Symbol<Character> symbol1 = (Symbol<Character>) SpecContext.getInstance().context.get(name1);
        Symbol<Character> symbol2 = (Symbol<Character>) SpecContext.getInstance().context.get(name2);
        boolean result = symbol1.equals(symbol2);
        SpecContext.getInstance().context.put("result", result);
    }

    @Then("^they should be equivalent$")
    public void theyShouldBeEquivalent() throws Throwable {
        boolean result = (boolean)SpecContext.getInstance().context.get("result");
        Assert.assertTrue(result);
    }

    @Then("^they should not be equivalent$")
    public void theyShouldNotBeEquivalent() throws Throwable {
        boolean result = (boolean)SpecContext.getInstance().context.get("result");
        Assert.assertFalse(result);
    }
}
