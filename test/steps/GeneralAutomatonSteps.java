package steps;

import com.jcespinoza.logic.IAutomaton;
import com.jcespinoza.logic.State;
import com.jcespinoza.logic.StateTransition;
import com.jcespinoza.logic.Symbol;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import spec.SpecContext;
import spec.SpecUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcespinoza on 7/24/2016.
 */
public class GeneralAutomatonSteps {
    @Given("^an existing \"([^\"]*)\" automaton with the alphabet \"([^\"]*)\"$")
    public void anExistingAutomatonWithTheAlphabet(String automatonType, String alphabet) throws Throwable {
        SpecContext.initializeAutomaton(automatonType);
        IAutomaton<Character> automaton = SpecContext.Automaton;

        char[] characters = alphabet.toCharArray();
        ArrayList<Symbol<Character>> symbols = new ArrayList<>();
        for(Character character : characters){
            Symbol<Character> symbol = new Symbol<>(character);
            symbols.add(symbol);
        }
        automaton.setAlphabet(symbols);
    }

    @And("^a state \"([^\"]*)\"$")
    public void aState(String name) throws Throwable {
        State state = new State(name);
        SpecContext.Automaton.addState(state);
    }

    @And("^a transition from \"([^\"]*)\" to \"([^\"]*)\" with \"([^\"]*)\"$")
    public void aTransitionFromToWith(String originName, String targetName, String symbolName) throws Throwable {
        State originState = SpecContext.Automaton.getStateByName(originName);
        State destinationState = SpecContext.Automaton.getStateByName(targetName);
        Symbol<Character> symbol = new Symbol<>(symbolName.toCharArray()[0]);
        StateTransition<Character> transition = new StateTransition<>(originState, destinationState, symbol);

        SpecContext.Automaton.addStateTransition(transition);
    }

    @When("^I remove the state \"([^\"]*)\"$")
    public void iRemoveTheState(String name) throws Throwable {
        SpecContext.Automaton.removeState(name);
    }

    @Then("^it should have \"([^\"]*)\" states and \"([^\"]*)\" transitions$")
    public void itShouldHaveStatesAndTransitions(String statesCount, String transitionsCount) throws Throwable {
        int expectedNumberOfStates = SpecUtils.InterpretDecimalName(statesCount);
        int expectedNumberOfTransitions = SpecUtils.InterpretDecimalName(transitionsCount);

        int actualNumberOfStates = SpecContext.Automaton.getStates().size();
        int actualNumberOfTransition = SpecContext.Automaton.getTransitions().size();

        Assert.assertTrue("Number of states do not match", expectedNumberOfStates == actualNumberOfStates);
        Assert.assertTrue("Number of transitions do not match", expectedNumberOfTransitions == actualNumberOfTransition);
    }

    @Given("^an existing \"([^\"]*)\" automaton with no states$")
    public void anExistingAutomatonWithNoStates(String automatonType) throws Throwable {
        SpecContext.initializeAutomaton(automatonType);
    }

    @When("^I add a new state$")
    public void iAddANewState() throws Throwable {
        State newState = new State();
        SpecContext.Automaton.addState(newState);
    }

    @When("^I tell it to verify whether it accepts the symbol \"([^\"]*)\"$")
    public void iTellItToVerifyWhetherItAcceptsTheSymbol(String symbolString) throws Throwable {
        Symbol<Character> symbol = new Symbol<>(symbolString.toCharArray()[0]);
        boolean evaluationResult = SpecContext.Automaton.acceptsSymbol(symbol);
        SpecContext.getInstance().context.put("evaluationResult", evaluationResult);
    }

    @Then("^it should accept it$")
    public void itShouldAcceptIt() throws Throwable {
        boolean evaluationResult = (boolean)SpecContext.getInstance().context.get("evaluationResult");
        Assert.assertTrue ("Evaluation yielded the wrong result", evaluationResult);
    }

    @Then("^it should not accept it$")
    public void itShouldNotAcceptIt() throws Throwable {
        boolean evaluationResult = (boolean)SpecContext.getInstance().context.get("evaluationResult");
        Assert.assertFalse ("Evaluation yielded the wrong result", evaluationResult);
    }

    @And("^a start state \"([^\"]*)\"$")
    public void aStartState(String stateName) throws Throwable {
        State state = new State(stateName);
        SpecContext.Automaton.addState(state);
        SpecContext.Automaton.setStartState(state);
    }

    @And("^I set the state \"([^\"]*)\" as accepted state$")
    public void iSetTheStateAsAcceptedState(String stateName) throws Throwable {
        State state = SpecContext.Automaton.getStateByName(stateName);
        SpecContext.Automaton.addAcceptedState(state);
    }

    @When("^I evaluate the string \"([^\"]*)\"$")
    public void iEvaluateTheString(String inputString) throws Throwable {
        List<Symbol<Character>> symbols = new ArrayList<>();
        for(char character : inputString.toCharArray()){
            Symbol<Character> symbol = new Symbol<>(character);
            symbols.add(symbol);
        }
        boolean evaluationResult = SpecContext.Automaton.acceptsInput(symbols);
        SpecContext.getInstance().context.put( "evaluationResult", evaluationResult);
    }
}
