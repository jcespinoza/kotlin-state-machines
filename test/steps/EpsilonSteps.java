package steps;

import com.jcespinoza.logic.EpsilonNonDeterministicFiniteAutomaton;
import com.jcespinoza.logic.State;
import com.jcespinoza.logic.StateTransition;
import com.jcespinoza.logic.Symbol;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import spec.SpecContext;
import spec.SpecUtils;

import java.util.Set;

/**
 * Created by jcespinoza on 7/31/2016.
 */
public class EpsilonSteps {
    @And("^a transition from \"([^\"]*)\" to \"([^\"]*)\" with epsilon \"([^\"]*)\"$")
    public void aTransitionFromToWithEpsilon(String originName, String targetName, String symbolName) throws Throwable {
        State originState = SpecContext.Automaton.getStateByName(originName);
        State destinationState = SpecContext.Automaton.getStateByName(targetName);
        Symbol<Character> symbol = new Symbol<>(symbolName.toCharArray()[0], true);
        StateTransition<Character> transition = new StateTransition<>(originState, destinationState, symbol);

        SpecContext.Automaton.addStateTransition(transition);
    }

    @When("^I get the epsilon-closure for state \"([^\"]*)\"$")
    public void iGetTheEpsilonClosureForState(String stateName) throws Throwable {
        State state = SpecContext.Automaton.getStateByName(stateName);

        Set<State> resultSet = ((EpsilonNonDeterministicFiniteAutomaton<Character>)(SpecContext.Automaton)).evaluateKleeneClosure(state);
        SpecContext.getInstance ().context.put("resultSet", resultSet);
    }

    @Then("^the result set should be of length \"([^\"]*)\"$")
    public void theResultSetShouldBeOfLength(String lengthName) throws Throwable {
        int expectedLength = SpecUtils.InterpretDecimalName(lengthName);

        Set<State> resultSet =  (Set<State>) SpecContext.getInstance().context.get("resultSet");

        Assert.assertTrue( "Result set count is incorrect ", expectedLength == resultSet.size());
    }

    @And("^the result set should have a state \"([^\"]*)\"$")
    public void theResultSetShouldHaveAState(String stateName) throws Throwable {
        Set<State> resultSet =  (Set<State>) SpecContext.getInstance().context.get("resultSet");

        boolean found = false;

        for(State st : resultSet){
            if( st.getStateName().equals(stateName)){
                found = true;
                break;
            }
        }

        Assert.assertTrue( "State not found ", found);
    }
}
