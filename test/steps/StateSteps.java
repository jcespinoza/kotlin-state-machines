package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.jcespinoza.logic.State;
import org.junit.Assert;
import spec.SpecContext;

/**
 * Created by jcespinoza on 7/24/2016.
 */
public class StateSteps {
    @Given("^I have a state \"([^\"]*)\" with name \"([^\"]*)\" and acceptedness \"([^\"]*)\"$")
    public void iHaveAStateWithNameAndAcceptedness(String id, String stateName, String acceptedness) throws Throwable {
        boolean accepted = acceptedness.equals("true");
        State state = new State(stateName, accepted);
        SpecContext.getInstance().context.put(id, state);
    }

    @When("^I compare the state \"([^\"]*)\" with state \"([^\"]*)\"$")
    public void iCompareTheStateWithState(String name1, String name2) throws Throwable {
        State state1 = (State)SpecContext.getInstance().context.get(name1);
        State state2 = (State)SpecContext.getInstance().context.get(name2);
        boolean result = state1.equals(state2);
        SpecContext.getInstance().context.put("result", result);
    }

    @Then("^the states should be equivalent$")
    public void theStatesShouldBeEquivalent() throws Throwable {
        boolean result = (boolean)SpecContext.getInstance().context.get("result");
        Assert.assertTrue(result);
    }

    @Then("^the states should not be equivalent$")
    public void theStatesShouldNotBeEquivalent() throws Throwable {
        boolean result = (boolean)SpecContext.getInstance().context.get("result");
        Assert.assertFalse(result);
    }
}
