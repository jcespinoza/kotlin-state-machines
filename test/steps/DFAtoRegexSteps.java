package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by jcespinoza on 8/19/2016.
 */
public class DFAtoRegexSteps {
    @When("^I try to convert it to a regular expression$")
    public void iTryToConvertItToARegularExpression() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the resulting regular expression should be \"([^\"]*)\"$")
    public void theResultingRegularExpressionShouldBe(String pattern) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
