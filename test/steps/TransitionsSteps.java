package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.jcespinoza.logic.State;
import com.jcespinoza.logic.StateTransition;
import com.jcespinoza.logic.Symbol;
import org.junit.Assert;
import spec.SpecContext;

/**
 * Created by jcespinoza on 7/24/2016.
 */
@SuppressWarnings("unchecked")
public class TransitionsSteps {
    @Given("^I have a transition \"([^\"]*)\" with origin \"([^\"]*)\" and target \"([^\"]*)\" and symbol \"([^\"]*)\"$")
    public void iHaveATransitionWithOriginAndTargetAndSymbol(String id, String originName, String targetName, String sym) throws Throwable {
        State originState = new State(originName);
        State targetState = new State(targetName);
        Symbol<Character> symbol = new Symbol<>(sym.charAt(0));
        StateTransition<Character> transition = new StateTransition<>(originState, targetState, symbol);
        SpecContext.getInstance().context.put(id, transition);
    }

    @When("^I compare the transition \"([^\"]*)\" with transition \"([^\"]*)\"$")
    public void iCompareTheTransitionWithTransition(String name1, String name2) throws Throwable {
        StateTransition<Character> transition1 = (StateTransition<Character>)SpecContext.getInstance().context.get(name1);
        StateTransition<Character> transition2 = (StateTransition<Character>)SpecContext.getInstance().context.get(name2);
        boolean result = transition1.equals(transition2);
        SpecContext.getInstance().context.put("result", result);
    }

    @Then("^The transitions should be equivalent$")
    public void theTransitionsShouldBeEquivalent() throws Throwable {
        boolean result = (boolean)SpecContext.getInstance().context.get("result");
        Assert.assertTrue(result);
    }

    @Then("^The transitions should not be equivalent$")
    public void theTransitionsShouldNotBeEquivalent() throws Throwable {
        boolean result = (boolean)SpecContext.getInstance().context.get("result");
        Assert.assertFalse(result);
    }
}
