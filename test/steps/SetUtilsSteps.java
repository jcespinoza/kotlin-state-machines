package steps;

import com.jcespinoza.utils.SetUtils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import spec.SpecContext;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by jcespinoza on 7/31/2016.
 */
public class SetUtilsSteps {
    @Given("^an empty set \"([^\"]*)\"$")
    public void anEmptySet(String name) throws Throwable {
        Set<String> set = new HashSet<>();
        SpecContext.getInstance().context.put(name, set);
    }

    @And("^that I added the element \"([^\"]*)\" to set \"([^\"]*)\"$")
    public void thatIAddedTheElementToSet(String element, String setName) throws Throwable {
        Set<String> set = (Set<String>)SpecContext.getInstance().context.get(setName);
        set.add(element);
        SpecContext.getInstance().context.put(setName, set);
    }

    @When("^I calculate the Union of set \"([^\"]*)\" and set \"([^\"]*)\"$")
    public void iCalculateTheUnionOfSetAndSet(String setNameA, String setNameB) throws Throwable {
        Set<String> setA = (Set<String>)SpecContext.getInstance().context.get(setNameA);
        Set<String> setB = (Set<String>)SpecContext.getInstance().context.get(setNameB);
        Set<String> resultSet = SetUtils.INSTANCE.Union(setA, setB);
        SpecContext.getInstance().context.put("resultSet", resultSet);
    }

    @When("^I calculate the Intersection of set \"([^\"]*)\" and set \"([^\"]*)\"$")
    public void iCalculateTheIntersectionOfSetAndSet(String setNameA, String setNameB) throws Throwable {
        Set<String> setA = (Set<String>)SpecContext.getInstance().context.get(setNameA);
        Set<String> setB = (Set<String>)SpecContext.getInstance().context.get(setNameB);
        Set<String> resultSet = SetUtils.INSTANCE.Union(setA, setB);
        SpecContext.getInstance().context.put("resultSet", resultSet);
    }

    @When("^I calculate the Difference of set \"([^\"]*)\" and set \"([^\"]*)\"$")
    public void iCalculateTheDifferenceOfSetAndSet(String setNameA, String setNameB) throws Throwable {
        Set<String> setA = (Set<String>)SpecContext.getInstance().context.get(setNameA);
        Set<String> setB = (Set<String>)SpecContext.getInstance().context.get(setNameB);
        Set<String> resultSet = SetUtils.INSTANCE.Difference(setA, setB);
        SpecContext.getInstance().context.put("resultSet", resultSet);
    }
    @Then("^it should give a new set with the same elements of set \"([^\"]*)\"$")
    public void itShouldGiveANewSetWithTheSameElementsOfSet(String setName) throws Throwable {
        Set<String> setFound = (Set<String>)SpecContext.getInstance().context.get(setName);
        Set<String> resultSet = (Set<String>)SpecContext.getInstance().context.get("resultSet");

        Assert.assertTrue("Size of sets differ",setFound.size() == resultSet.size());
        for(String element : setFound){
            Assert.assertTrue("Set elements differ", resultSet.contains(element));
        }
    }

    @Then("^it should give a new set with the common elements of set \"([^\"]*)\" and set \"([^\"]*)\" without repeating plus the uncommon$")
    public void itShouldGiveANewSetWithTheCommonElementsOfSetAndSetWithoutRepeatingPlusTheUncommon(String setAName, String setBName) throws Throwable {
        Set<String> setA = (Set<String>)SpecContext.getInstance().context.get(setAName);
        Set<String> setB = (Set<String>)SpecContext.getInstance().context.get(setBName);
        Set<String> resultSet = (Set<String>)SpecContext.getInstance().context.get("resultSet");

        for(String element : setA){
            if(setB.contains(element)) {
                Assert.assertTrue("Result Set does not contain all common elements", resultSet.contains(element));
            }
        }
        for(String element : setB){
            if(setA.contains(element)) {
                Assert.assertTrue("Result Set does not contain all common elements", resultSet.contains(element));
            }
        }
    }

    @Then("^it should give a new set containing only once the elements set \"([^\"]*)\" and set \"([^\"]*)\" have in common$")
    public void itShouldGiveANewSetContainingOnlyOnceTheElementsSetAndSetHaveInCommon(String setAName, String setBName) throws Throwable {
        itShouldGiveANewSetWithTheCommonElementsOfSetAndSetWithoutRepeatingPlusTheUncommon(setAName, setBName);
    }

    @Then("^it should give a new set with the elements of set \"([^\"]*)\" that are not in \"([^\"]*)\"$")
    public void itShouldGiveANewSetWithTheElementsOfSetThatAreNotIn(String setAName, String setBName) throws Throwable {
        Set<String> setA = (Set<String>)SpecContext.getInstance().context.get(setAName);
        Set<String> setB = (Set<String>)SpecContext.getInstance().context.get(setBName);
        Set<String> resultSet = (Set<String>)SpecContext.getInstance().context.get("resultSet");

        for(String element : setA){
            if(resultSet.contains(element))
                Assert.assertTrue( "Sets have elements in common", !setB.contains(element));
        }    }

    @Given("^a List of elements \"([^\"]*)\"$")
    public void aListOfElements(String name) throws Throwable {
        List<String> list = new ArrayList<>();
        SpecContext.getInstance().context.put(name, list);
    }

    @When("^I convert the array \"([^\"]*)\" to a set$")
    public void iConvertTheArrayToASet(String listName) throws Throwable {
        List<String> list = (List<String>)SpecContext.getInstance().context.get(listName);

        Set<String> resultSet = SetUtils.INSTANCE.ToSet(list);
        SpecContext.getInstance().context.put("resultSet", resultSet);
    }

    @Then("^it should give a new set with the same elements the list \"([^\"]*)\" had$")
    public void itShouldGiveANewSetWithTheSameElementsTheListHad(String listName) throws Throwable {
        List<String> listFound = (List<String>)SpecContext.getInstance().context.get(listName);
        Set<String> resultSet = (Set<String>)SpecContext.getInstance().context.get("resultSet");

        Assert.assertTrue("Size of sets differ", listFound.size() == resultSet.size());
        for(String element : listFound){
            Assert.assertTrue("Set elements differ", resultSet.contains(element));
        }
    }
}
