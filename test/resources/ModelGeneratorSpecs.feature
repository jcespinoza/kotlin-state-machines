# Created by jcespinoza at 8/4/2016
Feature: Generation of Model for further serialization
  In order to serialize a user drawn automaton
  As a computer science student
  I need a way to convert from the graphical representation to a serializable model

  Scenario: Serialization of simple DFA automaton
    Given an existing "DFA" automaton with the alphabet "ab"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And a transition from "A" to "B" with "a"
    And a transition from "B" to "C" with "a"
    And a transition from "B" to "D" with "b"
    And a transition from "C" to "B" with "a"
    And a transition from "C" to "D" with "b"
    And a transition from "D" to "B" with "a"
    And I set the state "C" as accepted state
    And I set the state "D" as accepted state
    And an existing graph "G1"
    And the following vertices in the graph "G1"
      | name  | x     | y     |
      | A     | 1.0   | 1.0   |
      | B     | 2.0   | 2.0   |
      | C     | 3.0   | 3.0   |
      | D     | 4.0   | 4.0   |
    When I generate an automaton model "G1"
    Then the resulting model should have these properties
      | name | type | epsilon |
      | G1   | DFA  | ε       |
    And the resulting model should have these states
      | name | accepted |   x   |  y  |
      | A    | false    | 1.0   | 1.0 |
      | B    | false    | 2.0   | 2.0 |
      | C    | true     | 3.0   | 3.0 |
      | D    | true     | 4.0   | 4.0 |
    And the resulting model should have the following initial state
      | name | accepted |   x   |  y  |
      | A    | false    | 1.0   | 1.0 |
    And the resulting model should have the following accepted states
      | name | accepted |   x   | y   |
      | C    | true     | 3.0   | 3.0 |
      | D    | true     | 4.0   | 4.0 |
    And the resulting model should have the following alphabet
      |   a    |
      |   b    |
    And the resulting model should have the following transitions
      | symbol | source | target |
      | a      | A      | B      |
      | a      | B      | C      |
      | b      | B      | D      |
      | a      | C      | B      |
      | b      | C      | D      |
      | a      | D      | B      |