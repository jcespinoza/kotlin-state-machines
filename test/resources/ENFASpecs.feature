# Created by jcespinoza at 7/31/2016
#noinspection SpellCheckingInspection
Feature: Non-deterministic Finite State Automaton with Epsilon Transitions
  In order to determine if a e-NFA automaton is correct
  As an Automaton theory student
  I want to construct it and test input strings

  Scenario: Add a state to an Automaton
    Given an existing "eNFA" automaton with no states
    When I add a new state
    Then it should have "one" states and "no" transitions

  Scenario: Checking for valid Symbols
    Given an existing "eNFA" automaton with the alphabet "abc"
    When I tell it to verify whether it accepts the symbol "b"
    Then it should accept it

  Scenario: Checking for valid Symbols
    Given an existing "eNFA" automaton with the alphabet "abc"
    When I tell it to verify whether it accepts the symbol "ε"
    Then it should accept it

  Scenario: Rejecting invalid Symbols
    Given an existing "eNFA" automaton with the alphabet "abc"
    When I tell it to verify whether it accepts the symbol "e"
    Then it should not accept it

  Scenario: Calculating the epsilon closure for a tree of states
    Given an existing "eNFA" automaton with the alphabet "10"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And I set the state "C" as accepted state
    And a transition from "A" to "B" with epsilon "ε"
    And a transition from "B" to "C" with "1"
    And a transition from "B" to "B" with "0"
    And a transition from "B" to "D" with epsilon "ε"
    And a transition from "C" to "A" with epsilon "ε"
    And a transition from "D" to "C" with "0"
    And a transition from "D" to "C" with "1"
    When I get the epsilon-closure for state "A"
    Then the result set should be of length "3"
    And the result set should have a state "B"
    And the result set should have a state "D"

  Scenario: Evaluating an acceptable string
    Given an existing "eNFA" automaton with the alphabet "10"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And I set the state "C" as accepted state
    And a transition from "A" to "B" with epsilon "ε"
    And a transition from "B" to "C" with "1"
    And a transition from "B" to "B" with "0"
    And a transition from "B" to "D" with epsilon "ε"
    And a transition from "C" to "A" with epsilon "ε"
    And a transition from "D" to "C" with "0"
    And a transition from "D" to "C" with "1"
    When I evaluate the string "000101"
    Then it should accept it

  Scenario: Evaluating an acceptable string
    Given an existing "eNFA" automaton with the alphabet "10"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And I set the state "C" as accepted state
    And a transition from "A" to "B" with epsilon "ε"
    And a transition from "B" to "C" with "1"
    And a transition from "B" to "B" with "0"
    And a transition from "B" to "D" with epsilon "ε"
    And a transition from "C" to "A" with epsilon "ε"
    And a transition from "D" to "C" with "0"
    And a transition from "D" to "C" with "1"
    When I evaluate the string "000100"
    Then it should accept it

    Scenario: Machine where an extra Kleene closure is needed 1
      Given an existing "eNFA" automaton with the alphabet "10"
      And a start state "q0"
      And a state "q1"
      And a state "q2"
      And a state "q3"
      And I set the state "q3" as accepted state
      And a transition from "q0" to "q1" with epsilon "0"
      And a transition from "q0" to "q2" with epsilon "ε"
      And a transition from "q1" to "q2" with epsilon "1"
      And a transition from "q2" to "q3" with epsilon "1"
      And a transition from "q1" to "q3" with epsilon "ε"
      When I evaluate the string "0"
      Then it should accept it

  Scenario: Machine where an extra Kleene closure is needed 2
    Given an existing "eNFA" automaton with the alphabet "10"
    And a start state "q0"
    And a state "q1"
    And a state "q2"
    And a state "q3"
    And I set the state "q3" as accepted state
    And a transition from "q0" to "q1" with epsilon "0"
    And a transition from "q0" to "q2" with epsilon "ε"
    And a transition from "q1" to "q2" with epsilon "1"
    And a transition from "q2" to "q3" with epsilon "1"
    And a transition from "q1" to "q3" with epsilon "ε"
    When I evaluate the string "1"
    Then it should accept it