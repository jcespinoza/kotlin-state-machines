# Created by jcespinoza at 7/24/2016
Feature: Transition Equality
  As a developer
  In order to properly evaluate state machines
  I need to make sure I know if two transitions are equivalent

  Scenario: Comparing transition to itself
    Given I have a transition "A" with origin "q0" and target "q1" and symbol "0"
    When I compare the transition "A" with transition "A"
    Then The transitions should be equivalent

  Scenario: Comparing transition to a similar one
    Given I have a transition "A" with origin "q0" and target "q1" and symbol "0"
    And I have a transition "B" with origin "q0" and target "q1" and symbol "0"
    When I compare the transition "A" with transition "B"
    Then The transitions should be equivalent

  Scenario: Comparing transition to a similar one who's symbol differs
    Given I have a transition "A" with origin "q0" and target "q1" and symbol "0"
    And I have a transition "B" with origin "q0" and target "q1" and symbol "1"
    When I compare the transition "A" with transition "B"
    Then The transitions should not be equivalent

  Scenario: Comparing transition to a similar one who's target differs
    Given I have a transition "A" with origin "q0" and target "q1" and symbol "0"
    And I have a transition "B" with origin "q0" and target "q2" and symbol "0"
    When I compare the transition "A" with transition "B"
    Then The transitions should not be equivalent

  Scenario: Comparing transition to a similar one who's source differs
    Given I have a transition "A" with origin "q0" and target "q1" and symbol "0"
    And I have a transition "B" with origin "q2" and target "q1" and symbol "0"
    When I compare the transition "A" with transition "B"
    Then The transitions should not be equivalent