# Created by jcespinoza at 7/24/2016
#noinspection SpellCheckingInspection

Feature: General Finite State Automaton
  In order to determine if any type of automaton works right
  As an Automaton theory student
  I want to construct it and check its behavior

  Scenario: Remove a state from an Automaton
    Given an existing "DFA" automaton with the alphabet "abc"
    And a state "I0"
    And a state "I1"
    And a transition from "I0" to "I1" with "a"
    When I remove the state "I0"
    Then it should have "one" states and "no" transitions