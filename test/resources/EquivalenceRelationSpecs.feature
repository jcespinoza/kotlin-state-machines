#noinspection SpellCheckingInspection
Feature: EquivalenceRelation specs
  In order to work with automata minimization
  As a developer
  I want to have a way to describe the equivalence of two states

  Background:
    Given an existing two-element list

  Scenario: Comparing equality of the same EquivalenceRelation
    Given an EquivalenceRelation object
    And I set the state1 to a State "A"
    And I set the state2 to a State "B"
    And I set the areEquivalent to "false"
    And I add it to a two element list of objects
    And an EquivalenceRelation object
    And I set the state1 to a State "A"
    And I set the state2 to a State "B"
    And I set the areEquivalent to "false"
    And I add it to a two element list of objects
    When I compare both EquivalenceObjects
    Then the comparison result of EquivalenceObjects should be "true"

  Scenario: Comparing equality with inverted states EquivalenceRelation
    Given an EquivalenceRelation object
    And I set the state1 to a State "B"
    And I set the state2 to a State "A"
    And I set the areEquivalent to "false"
    And I add it to a two element list of objects
    And an EquivalenceRelation object
    And I set the state1 to a State "A"
    And I set the state2 to a State "B"
    And I set the areEquivalent to "false"
    And I add it to a two element list of objects
    When I compare both EquivalenceObjects
    Then the comparison result of EquivalenceObjects should be "true"

  Scenario: Comparing equality of a totally different EquivalenceRelation
    Given an EquivalenceRelation object
    And I set the state1 to a State "C"
    And I set the state2 to a State "A"
    And I set the areEquivalent to "false"
    And I add it to a two element list of objects
    And an EquivalenceRelation object
    And I set the state1 to a State "A"
    And I set the state2 to a State "B"
    And I set the areEquivalent to "false"
    And I add it to a two element list of objects
    When I compare both EquivalenceObjects
    Then the comparison result of EquivalenceObjects should be "false"