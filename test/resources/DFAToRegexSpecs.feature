# Created by jcespinoza at 8/19/2016
Feature: Conversion of DFA to Regular Expression
  In order to pass the fucking class
  As Computer Sciences student
  I need to be able to convert DFAs to a Regular Expression

  Scenario: Conversion of a simple DFA
    Given an existing "DFA" automaton with the alphabet "01"
    And a start state "A"
    And a state "B"
    And a state "C"
    And I set the state "C" as accepted state
    And a transition from "A" to "B" with epsilon "0"
    And a transition from "B" to "B" with epsilon "1"
    And a transition from "B" to "C" with epsilon "0"
    When I try to convert it to a regular expression
    Then the resulting regular expression should be "0(1)*0"

  Scenario: Conversion of a simple DFA
    Given an existing "DFA" automaton with the alphabet "01"
    And a start state "A"
    And a state "B"
    And a state "C"
    And I set the state "C" as accepted state
    And a transition from "A" to "B" with epsilon "0"
    And a transition from "B" to "B" with epsilon "1"
    And a transition from "B" to "C" with epsilon "0"
    When I try to convert it to a regular expression
    Then the resulting regular expression should be "0(1)*0"

  Scenario: Conversion of a complex DFA
    Given an existing "DFA" automaton with the alphabet "ab"
    And a start state "S"
    And a state "B"
    And a state "C"
    And a state "D"
    And a state "E"
    And I set the state "E" as accepted state
    And a transition from "S" to "C" with "b"
    And a transition from "S" to "B" with "a"
    And a transition from "C" to "C" with "b"
    And a transition from "C" to "B" with "a"
    And a transition from "B" to "D" with "b"
    And a transition from "B" to "B" with "a"
    And a transition from "D" to "B" with "a"
    And a transition from "D" to "E" with "b"
    And a transition from "E" to "B" with "a"
    And a transition from "E" to "C" with "b"
    When I try to convert it to a regular expression
    Then the resulting regular expression should be any of the following
    | (a(ba+a)*bb+b(b)*a(ba+a)*bb)(a(ba+a)*bb+a(ba+a)*bb)* |
    | ((b(b)*a+a)(a+(ba)*)*bb)((b(b)*a+a)(a+(ba)*)*bb)*    |
    | (a(a)*b(a(a)*b)*b+b(b)*(a(a)*b)(a(a)*b(a(a)*b)*b+b(b)*a(a)*b(a(a)*b)*b)* |
