# Created by jcespinoza at 7/24/2016
Feature: State Equality
  As a developer
  In order to properly evaluate state machines
  I need to make sure I know if two states are equivalent

  Scenario: Comparing state to itself
    Given I have a state "A" with name "q0" and acceptedness "false"
    When I compare the state "A" with state "A"
    Then the states should be equivalent

  Scenario: States have the same properties
    Given I have a state "A" with name "q0" and acceptedness "false"
    And I have a state "B" with name "q0" and acceptedness "false"
    When I compare the state "A" with state "B"
    Then the states should be equivalent

  Scenario: Same name but different acceptedness
    Given I have a state "A" with name "q0" and acceptedness "false"
    And I have a state "B" with name "q0" and acceptedness "true"
    When I compare the state "A" with state "B"
    Then the states should be equivalent

  Scenario: Different name but same acceptedness
    Given I have a state "A" with name "q0" and acceptedness "true"
    And I have a state "B" with name "q1" and acceptedness "true"
    When I compare the state "A" with state "B"
    Then the states should not be equivalent

  Scenario: Totatlly Different States
    Given I have a state "A" with name "q0" and acceptedness "false"
    And I have a state "B" with name "q1" and acceptedness "true"
    When I compare the state "A" with state "B"
    Then the states should not be equivalent