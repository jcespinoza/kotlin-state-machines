# Created by jcespinoza at 7/31/2016
#noinspection SpellCheckingInspection
Feature: Set Utilities
  In order to use results of Set operations in my code
  As a developer
  I want to have those operations to be available

  Scenario: Applying Union to two sets with equal elements
    Given an empty set "A"
    And an empty set "B"
    And that I added the element "1" to set "A"
    And that I added the element "2" to set "A"
    And that I added the element "3" to set "A"
    And that I added the element "4" to set "A"
    And that I added the element "4" to set "B"
    And that I added the element "2" to set "B"
    And that I added the element "1" to set "B"
    And that I added the element "3" to set "B"
    When I calculate the Union of set "A" and set "B"
    Then it should give a new set with the same elements of set "A"

  Scenario: Applying Union to two sets with an uncommon element
    Given an empty set "A"
    And an empty set "B"
    And that I added the element "1" to set "A"
    And that I added the element "2" to set "A"
    And that I added the element "3" to set "A"
    And that I added the element "4" to set "A"
    And that I added the element "4" to set "B"
    And that I added the element "2" to set "B"
    And that I added the element "1" to set "B"
    And that I added the element "5" to set "B"
    When I calculate the Union of set "A" and set "B"
    Then it should give a new set with the common elements of set "A" and set "B" without repeating plus the uncommon

  Scenario: Applying Intersection to two sets with equal elements
    Given an empty set "A"
    And an empty set "B"
    And that I added the element "1" to set "A"
    And that I added the element "2" to set "A"
    And that I added the element "3" to set "A"
    And that I added the element "4" to set "A"
    And that I added the element "4" to set "B"
    And that I added the element "2" to set "B"
    And that I added the element "1" to set "B"
    And that I added the element "3" to set "B"
    When I calculate the Intersection of set "A" and set "B"
    Then it should give a new set with the same elements of set "B"

  Scenario: Applying Intersection to two sets with unequal elements
    Given an empty set "A"
    And an empty set "B"
    And that I added the element "1" to set "A"
    And that I added the element "2" to set "A"
    And that I added the element "3" to set "A"
    And that I added the element "4" to set "A"
    And that I added the element "4" to set "B"
    And that I added the element "2" to set "B"
    And that I added the element "1" to set "B"
    And that I added the element "5" to set "B"
    When I calculate the Intersection of set "A" and set "B"
    Then it should give a new set containing only once the elements set "A" and set "B" have in common

  Scenario: Applying Difference to two sets with equal elements
    Given an empty set "A"
    And an empty set "B"
    And that I added the element "1" to set "A"
    And that I added the element "2" to set "A"
    And that I added the element "3" to set "A"
    And that I added the element "4" to set "A"
    And that I added the element "4" to set "B"
    And that I added the element "2" to set "B"
    And that I added the element "1" to set "B"
    And that I added the element "3" to set "B"
    When I calculate the Difference of set "A" and set "B"
    Then it should give a new set with the elements of set "A" that are not in "B"
    Then it should give a new set with the elements of set "B" that are not in "A"

  Scenario: Applying Difference to two sets with unequal elements
    Given an empty set "A"
    And an empty set "B"
    And that I added the element "1" to set "A"
    And that I added the element "2" to set "A"
    And that I added the element "3" to set "A"
    And that I added the element "4" to set "A"
    And that I added the element "4" to set "B"
    And that I added the element "2" to set "B"
    And that I added the element "1" to set "B"
    And that I added the element "5" to set "B"
    When I calculate the Difference of set "A" and set "B"
    Then it should give a new set with the elements of set "A" that are not in "B"
    Then it should give a new set with the elements of set "B" that are not in "A"

  Scenario: Converting an Array List to a set
    Given a List of elements "A"
    When I convert the array "A" to a set
    Then it should give a new set with the same elements the list "A" had