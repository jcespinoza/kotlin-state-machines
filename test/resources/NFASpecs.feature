# Created by jcespinoza at 7/31/2016
#noinspection SpellCheckingInspection

Feature: Non-deterministic Finite State Automaton
  In order to determine if a NFA automaton is correct
  As an Automaton theory student
  I want to construct it and test input strings

  Scenario: Add a state to an Automaton
    Given an existing "NFA" automaton with no states
    When I add a new state
    Then it should have "one" states and "no" transitions

  Scenario: Checking for valid Symbols
    Given an existing "NFA" automaton with the alphabet "abc"
    When I tell it to verify whether it accepts the symbol "b"
    Then it should accept it

  Scenario: Rejecting invalid Symbols
    Given an existing "NFA" automaton with the alphabet "abc"
    When I tell it to verify whether it accepts the symbol "e"
    Then it should not accept it

  Scenario: Testing an invalid string of symbols
    Given an existing "NFA" automaton with the alphabet "abc"
    And a start state "I0"
    And a state "I1"
    And a state "I2"
    And a state "I3"
    And a transition from "I0" to "I1" with "a"
    And a transition from "I0" to "I2" with "b"
    And a transition from "I0" to "I0" with "c"
    And a transition from "I1" to "I3" with "b"
    And a transition from "I2" to "I3" with "a"
    And a transition from "I1" to "I1" with "a"
    And a transition from "I1" to "I1" with "c"
    And a transition from "I2" to "I2" with "b"
    And a transition from "I2" to "I2" with "c"
    And a transition from "I3" to "I3" with "a"
    And a transition from "I3" to "I3" with "b"
    And a transition from "I3" to "I3" with "c"
    And I set the state "I3" as accepted state
    When I evaluate the string "abd"
    Then it should not accept it

  Scenario: Testing a valid string of symbols
    Given an existing "NFA" automaton with the alphabet "abc"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And a transition from "A" to "B" with "a"
    And a transition from "B" to "C" with "a"
    And a transition from "B" to "D" with "b"
    And a transition from "C" to "B" with "a"
    And a transition from "C" to "D" with "b"
    And a transition from "D" to "B" with "a"
    And I set the state "C" as accepted state
    And I set the state "D" as accepted state
    When I evaluate the string "aabab"
    Then it should accept it

  Scenario: Testing an almost-valid string of symbols
    Given an existing "NFA" automaton with the alphabet "ab"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And a transition from "A" to "B" with "a"
    And a transition from "B" to "C" with "a"
    And a transition from "B" to "D" with "b"
    And a transition from "C" to "B" with "a"
    And a transition from "C" to "D" with "b"
    And a transition from "D" to "B" with "a"
    And I set the state "C" as accepted state
    And I set the state "D" as accepted state
    When I evaluate the string "aabb"
    Then it should not accept it

  Scenario: Evaluating invalid strings. Simple Example.
    Given an existing "NFA" automaton with the alphabet "01"
    And a start state "I0"
    And a state "I1"
    And a transition from "I0" to "I0" with "0"
    And a transition from "I0" to "I0" with "1"
    And a transition from "I0" to "I1" with "0"
    And I set the state "I1" as accepted state
    When I evaluate the string "0110001"
    Then it should not accept it

  Scenario: Evaluating valid strings. Simple example.
    Given an existing "NFA" automaton with the alphabet "01"
    And a start state "I0"
    And a state "I1"
    And a transition from "I0" to "I0" with "0"
    And a transition from "I0" to "I0" with "1"
    And a transition from "I0" to "I1" with "0"
    And I set the state "I1" as accepted state
    When I evaluate the string "10101100"
    Then it should accept it

  Scenario: Evaluating invalid strings. Moderate example.
    Given an existing "NFA" automaton with the alphabet "abc"
    And a start state "I0"
    And a state "I1"
    And a state "I2"
    And a state "I3"
    And a state "I4"
    And a transition from "I0" to "I0" with "a"
    And a transition from "I0" to "I0" with "b"
    And a transition from "I0" to "I1" with "b"
    And a transition from "I0" to "I1" with "c"
    And a transition from "I1" to "I2" with "a"
    And a transition from "I1" to "I3" with "b"
    And a transition from "I2" to "I4" with "a"
    And a transition from "I2" to "I4" with "c"
    And a transition from "I3" to "I3" with "c"
    And a transition from "I3" to "I2" with "c"
    And I set the state "I2" as accepted state
    And I set the state "I4" as accepted state
    When I evaluate the string "abbcab"
    Then it should not accept it

  Scenario: Evaluation valid strings. Moderate example.
    Given an existing "NFA" automaton with the alphabet "abc"
    And a start state "I0"
    And a state "I1"
    And a state "I2"
    And a state "I3"
    And a state "I4"
    And a transition from "I0" to "I0" with "a"
    And a transition from "I0" to "I0" with "b"
    And a transition from "I0" to "I1" with "b"
    And a transition from "I0" to "I1" with "c"
    And a transition from "I1" to "I2" with "a"
    And a transition from "I1" to "I3" with "b"
    And a transition from "I2" to "I4" with "a"
    And a transition from "I2" to "I4" with "c"
    And a transition from "I3" to "I3" with "c"
    And a transition from "I3" to "I2" with "c"
    And I set the state "I2" as accepted state
    And I set the state "I4" as accepted state
    When I evaluate the string "abbbbca"
    Then it should accept it