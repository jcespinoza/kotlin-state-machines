# Created by jcespinoza at 8/7/2016
Feature: Decoding of Model from previous deserialization
  In order to create an automaton from a file
  As a computer science student
  I need a way to convert from a serialized model to a graphical representation

  Scenario: Decoding of simple DFA model
    Given an existing automaton model "S1" with the following properties
      | name | type | epsilon |
      | S1   | DFA  | ε       |
    And the following state is assigned as initial to "S1"
      | name | accepted |   x   |  y  |
      | s1   | true     | 0.0   | 1.2 |
    And the following alphabet model is assigned to "S1"
      |   0    |
      |   1    |
    And the following state models are assigned to "S1"
      | name | accepted |   x   |  y  |
      | s1   | true     | 0.0   | 1.2 |
      | s2   | false    | 2.1   | 2.6 |
    And the following states are assigned as final states for "S1"
      | name | accepted |   x   | y   |
      | s1   | true     | 0.0   | 1.2 |
    And the following transition models are assigned to "S1"
      | symbol | source  | target  |
      | 0      | s1      | s2      |
      | 1      | s1      | s1      |
      | 1      | s2      | s2      |
      | 0      | s2      | s1      |
    And an existing graph "G1"
    When I decode the model "S1" to an automaton with graph "G1"
    Then it should have a start state "s1"
    And the resulting automaton should have "2" states and "4" transitions
    And it should have a state "s2"
    And it should have a final state "s1"
    And it should have the following alphabet
      | 1  |
      | 0  |
    And it should have a transition from "s1" to "s1" with "1"
    And it should have a transition from "s1" to "s2" with "0"
    And it should have a transition from "s2" to "s2" with "1"
    And it should have a transition from "s2" to "s1" with "0"
    And the graph "G1" should have "2" states
    And the graph "G1" should have this vertices
      | name |
      | s1   |
      | s2   |