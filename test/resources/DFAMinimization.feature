#noinspection SpellCheckingInspection
Feature: Minimization of Deterministic Finite State Automata
  In order to minimize DFAs
  As an Automata theory student
  I want to construct hem and test input strings

  Scenario: Minimization of invalid DFA
    Given an existing "DFA" automaton with no states
    When I try to minimize it
    Then it should throw an error since it is invalid

  Scenario: Minimization of simple DFA
    Given an existing "DFA" automaton with the alphabet "01"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And a state "E"
    And a state "F"
    And I set the state "F" as accepted state
    And a transition from "A" to "B" with "0"
    And a transition from "A" to "E" with "1"
    And a transition from "B" to "B" with "0"
    And a transition from "E" to "B" with "0"
    And a transition from "B" to "D" with "1"
    And a transition from "E" to "C" with "1"
    And a transition from "D" to "F" with "0"
    And a transition from "C" to "F" with "0"
    When I try to minimize it
    Then the resulting automaton should have "4" states and "5" transitions
    And both the original and resulting automaton should accept the string "100010"
    And both the original and resulting automaton should reject the string "0110"

#  Scenario: Minimization of tough DFA
#    Given an existing "DFA" automaton with the alphabet "01"
#    And a start state "A"
#    And a state "B"
#    And a state "C"
#    And I set the state "A" as accepted state
#    And I set the state "B" as accepted state
#    And I set the state "C" as accepted state
#    And a transition from "A" to "B" with "0"
#    And a transition from "B" to "C" with "0"
#    And a transition from "C" to "A" with "0"
#    When I try to minimize it
#    Then the resulting automaton should have "1" states and "1" transitions
#    And both the original and resulting automaton should accept the string "0000"
#    And both the original and resulting automaton should reject the string "0"
#    And both the original and resulting automaton should accept the string ""

  Scenario: Minimization of UAT DFA
    Given an existing "DFA" automaton with the alphabet "10"
    And a start state "A"
    And a state "B"
    And a state "C"
    And I set the state "B" as accepted state
    And I set the state "C" as accepted state
    And a transition from "A" to "B" with "1"
    And a transition from "A" to "C" with "0"
    And a transition from "B" to "B" with "1"
    And a transition from "C" to "C" with "0"
    And a transition from "B" to "C" with "0"
    And a transition from "C" to "B" with "1"
    When I try to minimize it
    Then both the original and resulting automaton should accept the string "1110100"
    And both the original and resulting automaton should reject the string ""
    And the resulting automaton should have "2" states and "4" transitions