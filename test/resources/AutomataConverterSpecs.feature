# Created by jcespinoza at 8/2/2016
#noinspection SpellCheckingInspection
Feature: Conversion between automaton types
  In order to determine convert automata
  As an Automaton theory student
  I want to construct it and test input strings

  Scenario: Conversion from invalid NFA to a DFA
    Given an existing "NFA" automaton with no states
    When I try to convert it to a "DFA"
    Then it should throw an error since it is invalid

  Scenario: Conversion from DFA to a DFA
    Given an existing "DFA" automaton with the alphabet "abc"
    And a start state "I0"
    And a state "I1"
    And a state "I2"
    And a state "I3"
    And a transition from "I0" to "I1" with "a"
    And a transition from "I0" to "I2" with "b"
    And a transition from "I0" to "I0" with "c"
    And a transition from "I1" to "I3" with "b"
    And a transition from "I2" to "I3" with "a"
    And a transition from "I1" to "I1" with "a"
    And a transition from "I1" to "I1" with "c"
    And a transition from "I2" to "I2" with "b"
    And a transition from "I2" to "I2" with "c"
    And a transition from "I3" to "I3" with "a"
    And a transition from "I3" to "I3" with "b"
    And a transition from "I3" to "I3" with "c"
    And I set the state "I3" as accepted state
    When I try to convert it to a "DFA"
    Then it should return the same automaton

  Scenario: Conversion from simple NFA to a DFA - States in result
    Given an existing "NFA" automaton with the alphabet "01"
    And a start state "q0"
    And a state "q1"
    And a state "q2"
    And a transition from "q0" to "q1" with "1"
    And a transition from "q1" to "q1" with "0"
    And a transition from "q1" to "q1" with "1"
    And a transition from "q1" to "q2" with "1"
    And I set the state "q2" as accepted state
    When I try to convert it to a "DFA"
    Then it should have a start state "{q0}"
    And it should have a state "{q1}"
    And it should have a final state "{q1,q2}"
    And it should have the following alphabet
    | 0 |
    | 1 |
    And it should have a transition from "{q0}" to "{q1}" with "1"
    And it should have a transition from "{q1}" to "{q1}" with "0"
    And it should have a transition from "{q1}" to "{q1,q2}" with "1"
    And it should have a transition from "{q1,q2}" to "{q1,q2}" with "1"
    And it should have a transition from "{q1,q2}" to "{q1}" with "0"

  Scenario: Conversion from simple NFA to a DFA - number of resulting states
    Given an existing "NFA" automaton with the alphabet "01"
    And a start state "q0"
    And a state "q1"
    And a state "q2"
    And a transition from "q0" to "q1" with "1"
    And a transition from "q1" to "q1" with "0"
    And a transition from "q1" to "q1" with "1"
    And a transition from "q1" to "q2" with "1"
    And I set the state "q2" as accepted state
    When I try to convert it to a "DFA"
    Then the resulting automaton should have "3" states and "5" transitions


  Scenario: Conversion from simple NFA to a DFA - acceptance of string
    Given an existing "NFA" automaton with the alphabet "01"
    And a start state "q0"
    And a state "q1"
    And a state "q2"
    And a transition from "q0" to "q1" with "1"
    And a transition from "q1" to "q1" with "0"
    And a transition from "q1" to "q1" with "1"
    And a transition from "q1" to "q2" with "1"
    And I set the state "q2" as accepted state
    When I try to convert it to a "DFA"
    Then both the original and resulting automaton should accept the string "100101"
    And both the original and resulting automaton should accept the string "101"

  Scenario: Conversion from simple NFA to a DFA - rejection of string
    Given an existing "NFA" automaton with the alphabet "01"
    And a start state "q0"
    And a state "q1"
    And a state "q2"
    And a transition from "q0" to "q1" with "1"
    And a transition from "q1" to "q1" with "0"
    And a transition from "q1" to "q1" with "1"
    And a transition from "q1" to "q2" with "1"
    And I set the state "q2" as accepted state
    When I try to convert it to a "DFA"
    Then both the original and resulting automaton should reject the string "01110"
    And both the original and resulting automaton should reject the string "101010"

  Scenario: Conversion from moderate NFA to a DFA
    Given an existing "NFA" automaton with the alphabet "01"
    And a start state "q0"
    And a state "q1"
    And a state "q2"
    And a state "q3"
    And a state "q4"
    And a transition from "q0" to "q1" with "1"
    And a transition from "q0" to "q2" with "0"
    And a transition from "q0" to "q3" with "0"
    And a transition from "q1" to "q4" with "1"
    And a transition from "q2" to "q2" with "1"
    And a transition from "q2" to "q2" with "0"
    And a transition from "q2" to "q4" with "0"
    And a transition from "q3" to "q4" with "1"
    And I set the state "q4" as accepted state
    When I try to convert it to a "DFA"
    Then it should have a start state "{q0}"
    And it should have a state "{q1}"
    And it should have a state "{q2}"
    And it should have a state "{q2,q3}"
    And it should have a state "{q2,q4}"
    And it should have a state "{q4}"
    And it should have a final state "{q2,q4}"
    And it should have a final state "{q4}"
    And it should have a transition from "{q0}" to "{q1}" with "1"
    And it should have a transition from "{q0}" to "{q2,q3}" with "0"
    And it should have a transition from "{q1}" to "{q4}" with "1"
    And it should have a transition from "{q2,q3}" to "{q2,q4}" with "1"
    And it should have a transition from "{q2,q3}" to "{q2,q4}" with "0"
    And it should have a transition from "{q2,q4}" to "{q2,q4}" with "0"
    And it should have a transition from "{q2,q4}" to "{q2}" with "1"
    And it should have a transition from "{q2}" to "{q2}" with "1"
    And it should have a transition from "{q2}" to "{q2,q4}" with "0"
    And the resulting automaton should have "6" states and "9" transitions
    And both the original and resulting automaton should reject the string "100101"
    Then both the original and resulting automaton should accept the string "011010"

  Scenario: Converting an eNFA to a DFA
    Given an existing "eNFA" automaton with the alphabet "10"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And I set the state "C" as accepted state
    And a transition from "A" to "B" with epsilon "ε"
    And a transition from "B" to "C" with "1"
    And a transition from "B" to "B" with "0"
    And a transition from "B" to "D" with epsilon "ε"
    And a transition from "C" to "A" with epsilon "ε"
    And a transition from "D" to "C" with "0"
    And a transition from "D" to "C" with "1"
    When I try to convert it to a "DFA"
    Then the resulting automaton should have "2" states and "4" transitions
    And it should have a start state "{A,B,D}"
    And it should have a final state "{A,B,C,D}"
    And it should have a transition from "{A,B,D}" to "{A,B,C,D}" with "0"
    And it should have a transition from "{A,B,D}" to "{A,B,C,D}" with "1"
    And it should have a transition from "{A,B,C,D}" to "{A,B,C,D}" with "0"
    And it should have a transition from "{A,B,C,D}" to "{A,B,C,D}" with "1"

  Scenario: Converting an UAT eNFA to a DFA
    Given an existing "eNFA" automaton with the alphabet "10"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And a state "E"
    And a state "F"
    And I set the state "F" as accepted state
    And a transition from "A" to "B" with epsilon "ε"
    And a transition from "A" to "C" with epsilon "ε"
    And a transition from "B" to "D" with "0"
    And a transition from "C" to "E" with "1"
    And a transition from "E" to "F" with epsilon "ε"
    And a transition from "D" to "F" with epsilon "ε"
    And a transition from "F" to "A" with epsilon "ε"
    When I try to convert it to a "DFA"
    Then the resulting automaton should have "3" states and "6" transitions
    Then both the original and resulting automaton should accept the string "1110100"
    Then both the original and resulting automaton should reject the string ""

  Scenario: Converting an UAT NFA to a DFA
    Given an existing "NFA" automaton with the alphabet "10"
    And a start state "A"
    And a state "B"
    And a state "C"
    And I set the state "C" as accepted state
    And a transition from "A" to "B" with "0"
    And a transition from "B" to "B" with "0"
    And a transition from "B" to "B" with "1"
    And a transition from "B" to "C" with "0"
    When I try to convert it to a "DFA"
    Then both the original and resulting automaton should reject the string "011"
    And both the original and resulting automaton should reject the string "1"
    And both the original and resulting automaton should accept the string "01110"
