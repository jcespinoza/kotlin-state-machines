# Created by jcespinoza at 8/2/2016
Feature: Automaton Serialization
  In order to work with Automata
  As a Computer Science student
  I need a way to serialize automata for further persistance

  Scenario: Serialization of an empty Automaton model
    Given an existing automaton model "A1" with the following properties
      | name | type | epsilon |
      | A1   | DFA  |         |
    When I serialize the automaton "A1"
    Then the resulting string should be
    """
    # meta
    name:A1
    type:DFA
    epsilon:
    initialState:
    # states
    # alphabet
    # final states
    # transitions

    """

  Scenario: Serialization of a complete Automaton model
    Given an existing automaton model "S1" with the following properties
    | name | type | epsilon |
    | S1   | DFA  | ε       |
    And the following state is assigned as initial to "S1"
    | name | accepted |   x   |  y  |
    | s1   | true     | 0.0   | 1.2 |
    And the following alphabet model is assigned to "S1"
    |   0    |
    |   1    |
    And the following state models are assigned to "S1"
    | name | accepted |   x   |  y  |
    | s1   | true     | 0.0   | 1.2 |
    | s2   | false    | 2.1   | 2.6 |
    And the following states are assigned as final states for "S1"
    | name | accepted |   x   | y   |
    | s1   | true     | 0.0   | 1.2 |
    And the following transition models are assigned to "S1"
    | symbol | source  | target  |
    | 0      | s1      | s2      |
    | 1      | s1      | s1      |
    | 1      | s2      | s2      |
    | 0      | s2      | s1      |
    When I serialize the automaton "S1"
    Then the resulting string should be
    """
    # meta
    name:S1
    type:DFA
    epsilon:ε
    initialState:s1
    # states
    name:s1:1:0.0,1.2
    name:s2:0:2.1,2.6
    # alphabet
    0
    1
    # final states
    s1
    # transitions
    0:s1>s2
    1:s1>s1
    1:s2>s2
    0:s2>s1

    """