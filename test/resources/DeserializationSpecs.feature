# Created by jcespinoza at 8/3/2016
Feature: Deserialization of Automata
  In order to work with automata
  As a computer sciences student
  I need to be able to retrieve saved models from disk

  Scenario: Deserialization of empty DFA model
    Given a file "A1.dfa" with the following contents
    """
    # meta
    name:A1
    type:DFA
    epsilon:
    initialState:
    # states
    # alphabet
    # final states
    # transitions

    """
    When I deserialize the file "A1.dfa"
    Then the resulting model should have these properties
    | name | type | epsilon |
    | A1   | DFA  |         |
    And the resulting model should have no states
    And the resulting model should have no initial state
    And the resulting model should have no accepted states
    And the resulting model should have no alphabet
    And the resulting model should have no transitions

  Scenario: Deserialization of complete DFA model
    Given a file "S1.dfa" with the following contents
    """
    # meta
    name:S1
    type:DFA
    epsilon:ε
    initialState:s1
    # states
    name:s1:1:0.0,1.2
    name:s2:0:2.1,2.6
    # alphabet
    0
    1
    # final states
    s1
    # transitions
    0:s1>s2
    1:s1>s1
    1:s2>s2
    0:s2>s1

    """
    When I deserialize the file "S1.dfa"
    Then the resulting model should have these properties
    | name | type | epsilon |
    | S1   | DFA  | ε       |
    And the resulting model should have these states
    | name | accepted |   x   |  y  |
    | s1   | true     | 0.0   | 1.2 |
    | s2   | false    | 2.1   | 2.6 |
    And the resulting model should have the following initial state
    | name | accepted |   x   |  y  |
    | s1   | true     | 0.0   | 1.2 |
    And the resulting model should have the following accepted states
    | name | accepted |   x   | y   |
    | s1   | true     | 0.0   | 1.2 |
    And the resulting model should have the following alphabet
    |   0    |
    |   1    |
    And the resulting model should have the following transitions
    | symbol | source  | target  |
    | 0      | s1      | s2      |
    | 1      | s1      | s1      |
    | 1      | s2      | s2      |
    | 0      | s2      | s1      |