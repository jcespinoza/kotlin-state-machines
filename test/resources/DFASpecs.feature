# Created by jcespinoza at 7/24/2016
#noinspection SpellCheckingInspection
Feature: Deterministic Finite State Automaton
  In order to determine if a DFA automaton is correct
  As an Automaton theory student
  I want to construct it and test input strings

  Scenario: Add a state to an Automaton
    Given an existing "DFA" automaton with no states
    When I add a new state
    Then it should have "one" states and "no" transitions

  Scenario: Checking for valid Symbols
    Given an existing "DFA" automaton with the alphabet "abc"
    When I tell it to verify whether it accepts the symbol "b"
    Then it should accept it

  Scenario: Rejecting invalid Symbols
    Given an existing "DFA" automaton with the alphabet "abc"
    When I tell it to verify whether it accepts the symbol "e"
    Then it should not accept it

  Scenario: Returning transitions for an existing state and a valid symbol from the alphabet
    Given an existing "DFA" automaton with the alphabet "abc"
    And a state "I0"
    And a state "I1"
    And a transition from "I0" to "I1" with "a"
    When I ask it for a transition with the symbol "a" for the state "I0"
    Then it should return only one transition with "I0" to "I1"

  Scenario: Returning transitions for an existing state and a invalid symbol from the alphabet
    Given an existing "DFA" automaton with the alphabet "abc"
    And a state "I0"
    And a state "I1"
    And a transition from "I0" to "I1" with "a"
    When I ask it for a transition with the symbol "d" for the state "I0"
    Then it should return no states

  Scenario: Adding a transition for a state with the same symbol
    Given an existing "DFA" automaton with the alphabet "abc"
    And a state "I0"
    And a state "I1"
    And a state "I2"
    And a transition from "I0" to "I1" with "a"
    When I try to add a transition from state "I0" to state "I2" with the symbol "a"
    Then it should not have two outputs from "I0" with the symbol "a"

  Scenario: Testing an invalid string of symbols
    Given an existing "DFA" automaton with the alphabet "abc"
    And a start state "I0"
    And a state "I1"
    And a state "I2"
    And a state "I3"
    And a transition from "I0" to "I1" with "a"
    And a transition from "I0" to "I2" with "b"
    And a transition from "I0" to "I0" with "c"
    And a transition from "I1" to "I3" with "b"
    And a transition from "I2" to "I3" with "a"
    And a transition from "I1" to "I1" with "a"
    And a transition from "I1" to "I1" with "c"
    And a transition from "I2" to "I2" with "b"
    And a transition from "I2" to "I2" with "c"
    And a transition from "I3" to "I3" with "a"
    And a transition from "I3" to "I3" with "b"
    And a transition from "I3" to "I3" with "c"
    And I set the state "I3" as accepted state
    When I evaluate the string "abd"
    Then it should not accept it

  Scenario: Testing a valid string of symbols
    Given an existing "DFA" automaton with the alphabet "ab"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And a transition from "A" to "B" with "a"
    And a transition from "B" to "C" with "a"
    And a transition from "B" to "D" with "b"
    And a transition from "C" to "B" with "a"
    And a transition from "C" to "D" with "b"
    And a transition from "D" to "B" with "a"
    And I set the state "C" as accepted state
    And I set the state "D" as accepted state
    When I evaluate the string "aabab"
    Then it should accept it

  Scenario: Testing an almost-valid string of symbols
    Given an existing "DFA" automaton with the alphabet "ab"
    And a start state "A"
    And a state "B"
    And a state "C"
    And a state "D"
    And a transition from "A" to "B" with "a"
    And a transition from "B" to "C" with "a"
    And a transition from "B" to "D" with "b"
    And a transition from "C" to "B" with "a"
    And a transition from "C" to "D" with "b"
    And a transition from "D" to "B" with "a"
    And I set the state "C" as accepted state
    And I set the state "D" as accepted state
    When I evaluate the string "aabb"
    Then it should not accept it