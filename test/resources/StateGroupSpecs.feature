# Created by jcespinoza at 8/2/2016
#noinspection SpellCheckingInspection
Feature: StateGroup specs
  In order to work with automata conversions
  As a developer
  I want to have a way to group many states into a single one

  Scenario: Creating a StateGroup with a single state
    Given an empty list of states
    And I add a state "A" to the list
    When I create a StateGroup
    Then the resulting StateGroup should have "1" states
    And the name of the resulting StateGroup should be "{A}"

  Scenario: Creating a StateGroup with two states
    Given an empty list of states
    And I add a state "A" to the list
    And I add a state "B" to the list
    When I create a StateGroup
    Then the resulting StateGroup should have "2" states
    And the name of the resulting StateGroup should be "{A,B}"

  Scenario: Creating a StateGroup out of many StateGroups with some common states
    Given an empty list of StateGroups
    Given an empty list of states
    And I add a state "A" to the list
    And I add a state "B" to the list
    And I create a StateGroup and add it to the list
    And an empty list of states
    And I add a state "B" to the list
    And I add a state "C" to the list
    And I create a StateGroup and add it to the list
    When I create a StateGroup out the StateGroups
    Then the resulting StateGroup should have "3" states
    And the name of the resulting StateGroup should be "{A,B,C}"

  Scenario: Creating a StateGroup out of many StateGroups with the same states
    Given an empty list of StateGroups
    Given an empty list of states
    And I add a state "A" to the list
    And I add a state "B" to the list
    And I create a StateGroup and add it to the list
    And an empty list of states
    And I add a state "B" to the list
    And I add a state "A" to the list
    And I create a StateGroup and add it to the list
    When I compare the first two StateGroups in the list
    Then the StateGroups should equal

  Scenario: Creating a StateGroup out of many StateGroups with the same states
    Given an empty list of StateGroups
    Given an empty list of states
    And I add a state "A" to the list
    And I add a state "B" to the list
    And I create a StateGroup and add it to the list
    And an empty list of states
    And I add a state "B" to the list
    And I add a state "A" to the list
    And I create a StateGroup and add it to the list
    When I compare the first two StateGroups' names in the list
    Then the StateGroups should have the same name