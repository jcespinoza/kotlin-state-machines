# Created by jcespinoza at 9/21/2016
Feature: Grammar to PDA Conversion
  In order to comprehend NonRegular languages
  As a Computer Science student
  I need to convert Grammars to PDAs

  Scenario: Conversion of Simple Grammar to PDA
    Given a grammar "G"
    And the following productions for the grammar "G"
    | Symbol | Production |
    | S | 0S1             |
    |