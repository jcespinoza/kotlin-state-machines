# Created by jcespinoza at 7/24/2016
Feature: Symbol Equality
  As a developer
  In order to properly evaluate state machines
  I need to make sure I know if two symbols are equivalent

  Scenario: Symbols are the same object
    Given I have one symbol named "A" with value "a"
    And I have one symbol named "B" with value "a"
    When I compare the symbols "A" and "B"
    Then they should be equivalent

  Scenario: Symbols are the different objects
    Given I have one symbol named "A" with value "a"
    And I have one symbol named "B" with value "b"
    When I compare the symbols "A" and "B"
    Then they should not be equivalent