package spec;

import com.jcespinoza.logic.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jcespinoza on 7/24/2016.
 */
public final class SpecContext {
    public Map<String, Object> context = new HashMap<>();
    private static SpecContext instance = new SpecContext();
    public static IAutomaton<Character> Automaton;

    public static SpecContext getInstance(){
        return instance;
    }

    public static void initializeAutomaton(String automatonType)
    {
        Automaton = CreateAutomaton(automatonType);
    }

    private static IAutomaton<Character> CreateAutomaton(String automatonType) {
        switch (automatonType){
            case "DFA" : return new DeterministicFiniteAutomaton<>();
            case "NFA" : return new NonDeterministicFiniteAutomaton<>();
            case "eNFA" : return new EpsilonNonDeterministicFiniteAutomaton<>(new Symbol<>('ε'));
        }
        throw new IllegalArgumentException("Invalid automaton type");
    }
    private SpecContext(){}
}
