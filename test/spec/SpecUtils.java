package spec;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jcespinoza on 7/24/2016.
 */
public final class SpecUtils {
    private static Map<String, Integer> numberNames = new HashMap<>();
    private static SpecUtils instance = new SpecUtils();

    public static SpecUtils getInstance(){
        return instance;
    }

    private SpecUtils(){
        numberNames.put("zero", 0);
        numberNames.put("one", 1);
        numberNames.put("two", 2);
        numberNames.put("three", 3);
        numberNames.put("four", 4);
        numberNames.put("five", 5);
        numberNames.put("six", 6);
        numberNames.put("seven", 7);
        numberNames.put("eight", 8);
        numberNames.put("nine", 9);
        numberNames.put("0", 0);
        numberNames.put("1", 1);
        numberNames.put("2", 2);
        numberNames.put("3", 3);
        numberNames.put("4", 4);
        numberNames.put("5", 5);
        numberNames.put("6", 6);
        numberNames.put("7", 7);
        numberNames.put("8", 8);
        numberNames.put("9", 9);
    }

    public static int InterpretDecimalName(String numberName)
    {
        if(numberName.equals("no")) return 0;
        int conversionResult;
        try {
            conversionResult = Integer.parseInt(numberName);
        } catch( NumberFormatException  e){
            conversionResult = numberNames.get(numberName);
        }
        return conversionResult;
    }

    public static boolean InterpretBoolean(String stringValue)
    {
        if(stringValue == null) return false;
        String loweredCase = stringValue.toLowerCase();
        return loweredCase.equals("1") ||
                loweredCase.equals("true") ||
                loweredCase.equals("yes");
    }
}
